//
//  SKPSMTPBookings.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/27/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "SKPSMTPMessage.h"
@class Booking;

typedef enum {
    RequestMethodAdd,
    RequestMethodCancel,
    RequestMethodEdit
}RequestMethod;

@interface SKPSMTPBookings : SKPSMTPMessage

@property (nonatomic, strong) Booking *uploadedBooking;
@property (nonatomic) RequestMethod method;

@end
