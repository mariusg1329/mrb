//
//  LDAPUser.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/21/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "LDAPUser.h"

@implementation LDAPUser

- (id)init
{
    if (self = [super init]) {
        self.LDAPUsername = @"";
        self.LDAPPassword = @"";
        self.isUsingLDAP = NO;
        self.isUsingOffline = NO;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.LDAPUsername = [decoder decodeObjectForKey:@"LDAPUsername"];
        self.LDAPPassword = [decoder decodeObjectForKey:@"LDAPPassword"];
        self.isUsingLDAP = [decoder decodeBoolForKey:@"isUsingLDAP"];
        self.isUsingOffline = [decoder decodeBoolForKey:@"isUsingOffline"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.LDAPUsername forKey:@"LDAPUsername"];
    [encoder encodeObject:self.LDAPPassword forKey:@"LDAPPassword"];
    [encoder encodeBool:self.isUsingLDAP forKey:@"isUsingLDAP"];
    [encoder encodeBool:self.isUsingOffline forKey:@"isUsingOffline"];
}

@end
