//
//  LoginViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingsManager.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate, SignInDelegate, UIGestureRecognizerDelegate>

@end
