//
//  AppointmentsViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "AppointmentViewController.h"
#import "Booking.h"
#import "EditableAppointmentViewController.h"
#import "NSMutableArray+Bookings.h"

@interface AppointmentViewController () {
    BOOL editingEnabled;
}

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UIButton *editButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *floorLabel;
@property (weak, nonatomic) IBOutlet UILabel *sizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *organizerLabel;
@property (weak, nonatomic) IBOutlet UILabel *attendeesLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backBtnHeight;

@end

@implementation AppointmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    if (editingEnabled) {
        self.editButton.hidden = NO;
        self.deleteButton.hidden = NO;
    } else {
        self.editButton.hidden = YES;
        self.deleteButton.hidden = YES;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setupData:self.currentBooking];
    [BookingsManager sharedInstance].bookingsDelegate = self;
    
    if ([[[UserManager sharedInstance] getActiveUser].userName isEqualToString:self.currentBooking.organizer]) {
        self.editButton.hidden = NO;
        self.deleteButton.hidden = NO;
    } else {
        self.editButton.hidden = YES;
        self.deleteButton.hidden = YES;
    }
    
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.tabBarController.selectedIndex == 0) {
        if (self.editButton.hidden == YES || [[BookingsManager sharedInstance].bookings containsBooking:self.currentBooking] == NO) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    editingEnabled = NO;
}

- (void)clearFields
{
    self.titleLabel.text = @"Location:";
    self.descriptionLabel.text = @"Description: ";
    self.locationLabel.text = @"Location:";
    self.floorLabel.text = @"Floor:";
    self.sizeLabel.text = @"Size:";
    self.dateLabel.text = @"Date:";
    self.organizerLabel.text = @"Organizer:";
    self.attendeesLabel.text = @"Attendees:";
}

- (void)setupData:(Booking*)booking
{
    [self clearFields];
    
    if (booking) {
        self.titleLabel.text = booking.title;
        self.locationLabel.text = [NSString stringWithFormat:@"%@ %@",self.locationLabel.text, booking.location.name];
        self.floorLabel.text = [NSString stringWithFormat:@"%@ %lu",self.floorLabel.text, booking.location.floor + 1];
        self.sizeLabel.text = [NSString stringWithFormat:@"%@ %@",self.sizeLabel.text, booking.location.size];
        self.dateLabel.text = [NSString stringWithFormat:@"%@ %@",self.dateLabel.text, [GeneralUtils daySuffixForDate:booking.startDate]];
        self.organizerLabel.text = booking.organizer;
        
        NSMutableString *attendees = [[NSMutableString alloc] init];
        for (int i = 0; i < booking.attendees.count; i++) {
            if (i == 0) {
                attendees = (NSMutableString*)[attendees stringByAppendingString:[NSString stringWithFormat:@"%@", [booking.attendees objectAtIndex:i]]];
                continue;
            }
            attendees = (NSMutableString*)[attendees stringByAppendingString:[NSString stringWithFormat:@"\n%@", [booking.attendees objectAtIndex:i]]];
        }
        
        self.attendeesLabel.text = attendees;
        self.descriptionLabel.text = [NSString stringWithFormat:@"%@ %@",self.descriptionLabel.text, booking.description];
        
        NSMutableAttributedString *attr = [[NSMutableAttributedString alloc] initWithString:self.locationLabel.text];
        
        [attr addAttribute:NSForegroundColorAttributeName value:self.organizerLabel.textColor range:NSMakeRange(10, self.locationLabel.text.length - 10)];
        [attr addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(10, self.locationLabel.text.length - 10)];

        self.locationLabel.attributedText = attr;
        
        NSMutableAttributedString *attr1 = [[NSMutableAttributedString alloc] initWithString:self.dateLabel.text];
        
        [attr1 addAttribute:NSForegroundColorAttributeName value:self.organizerLabel.textColor range:NSMakeRange(6, self.dateLabel.text.length - 6)];
        [attr1 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(6, self.dateLabel.text.length - 6)];
        
        self.dateLabel.attributedText = attr1;
        
        NSMutableAttributedString *attr2 = [[NSMutableAttributedString alloc] initWithString:self.floorLabel.text];
        
        [attr2 addAttribute:NSForegroundColorAttributeName value:self.organizerLabel.textColor range:NSMakeRange(7, self.floorLabel.text.length - 7)];
        [attr2 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(7, self.floorLabel.text.length - 7)];
        
        self.floorLabel.attributedText = attr2;
        
        NSMutableAttributedString *attr3 = [[NSMutableAttributedString alloc] initWithString:self.sizeLabel.text];
        
        [attr3 addAttribute:NSForegroundColorAttributeName value:self.organizerLabel.textColor range:NSMakeRange(6, self.sizeLabel.text.length - 6)];
        [attr3 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(6, self.sizeLabel.text.length - 6)];
        
        self.sizeLabel.attributedText = attr3;
        
        NSMutableAttributedString *attr4 = [[NSMutableAttributedString alloc] initWithString:self.descriptionLabel.text];
        
        [attr4 addAttribute:NSForegroundColorAttributeName value:self.organizerLabel.textColor range:NSMakeRange(13, self.descriptionLabel.text.length - 13)];
        [attr4 addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14.0f] range:NSMakeRange(13, self.descriptionLabel.text.length - 13)];
        
        self.descriptionLabel.attributedText = attr4;
    }
    
}

- (void)userDidEditBooking:(Booking *)booking
{
    [self setupData:booking];
    self.currentBooking = booking;
}

- (IBAction)deletePressed:(id)sender {
    [GeneralUtils showLoadingOn:self.tabBarController];
    [[BookingsManager sharedInstance] cancelBooking:self.currentBooking];
}

- (IBAction)editPressed:(id)sender {
    [self performSegueWithIdentifier:@"presentEditableAppointment" sender:self];
}

- (void)setEditingEnabled:(BOOL)isEditable
{
    editingEnabled = isEditable;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - WS Response

- (void)uploadFinished:(Booking *)booking
{
    [GeneralUtils hideLoadingOn:self.tabBarController];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)wsRequestError:(NSError *)error
{
    [GeneralUtils hideLoadingOn:self.tabBarController];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
    [alert show];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentEditableAppointment"]) {
        [segue.destinationViewController setBookingToEdit:self.currentBooking];
        [segue.destinationViewController setEditDelegate:self];
    }
}


@end
