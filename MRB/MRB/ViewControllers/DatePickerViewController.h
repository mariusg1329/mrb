//
//  DatePickerViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 28/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//


#import "CustomDatePicker+Date.h"

@protocol DatePickerDelegate <NSObject>

@optional
- (void)userDidPickDates:(NSArray*)dates;
- (void)userDidPickDate:(NSDate*)date;
- (void)userDidCancel;
- (void)handleDateChange:(NSDate*)date;

@end

@interface DatePickerViewController : UIViewController <CustomDatePickerDelegate>

@property (nonatomic, weak) IBOutlet CustomDatePicker *datePicker;
@property (nonatomic, weak) IBOutlet CustomDatePicker *datePickerEnd;

@property (nonatomic, weak) id<DatePickerDelegate> datePickerDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pickerType:(NSInteger)type selectedDate:(NSDate*)date minDate:(NSDate*)minDate;

@end
