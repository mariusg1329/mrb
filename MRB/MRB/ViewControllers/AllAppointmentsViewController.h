//
//  AllAppointmentsViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BookingsManager.h"
#import "DatePickerViewController.h"
#import "RoomPickerViewController.h"

@interface AllAppointmentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, DatePickerDelegate, UIGestureRecognizerDelegate, BookingsDelegate, UIAlertViewDelegate>

@end
