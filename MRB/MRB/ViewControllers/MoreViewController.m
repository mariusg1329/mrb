//
//  MoreViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/21/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "MoreViewController.h"
#import "ConfigurationManager.h"
#import "UserManager+Cache.h"

@interface MoreViewController () {
    BOOL keyboardIsOn;
    BOOL isOverlapping;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *credentialsHeight;
@property (weak, nonatomic) IBOutlet UITextField *ldapUsername;
@property (weak, nonatomic) IBOutlet UITextField *ldapPassword;
@property (weak, nonatomic) IBOutlet UISwitch *ldapSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *offlineSwitch;
@property (weak, nonatomic) IBOutlet UIView *ldapContainer;
@property (strong, nonatomic) IBOutlet UIButton *userStatusBtn;

@end

@implementation MoreViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    isOverlapping = [self willKeyboardOverlap];
    
    self.ldapUsername.delegate = self;
    self.ldapPassword.delegate = self;
    keyboardIsOn = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
}

- (void)setup
{
    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        
        if ([ConfigurationManager sharedInstance].currentLDAP.isUsingLDAP) {
            [self.ldapSwitch setOn:YES];
            [self toggleLDAPContainer];
        }
        
        LDAPUser *ldapUser = [LDAPUser new];
        
        ldapUser = [ConfigurationManager sharedInstance].currentLDAP;
        self.ldapUsername.text = ldapUser.LDAPUsername;
        self.ldapPassword.text = ldapUser.LDAPPassword;
        
        if ([ConfigurationManager sharedInstance].currentLDAP.isUsingOffline) {
            [self.offlineSwitch setOn:YES];
        }
        
    } else {
        self.ldapUsername.text = @"";
        self.ldapPassword.text = @"";
        [self.ldapSwitch setOn:NO];
        [self.offlineSwitch setOn:NO];
        [self toggleLDAPContainer];
    }
}

- (void)adjustFrame
{
    CGRect frame = self.view.frame;
    
    if (!keyboardIsOn) {
        frame.origin.y -= self.view.frame.size.height - self.ldapContainer.frame.size.height - KEYBOARD_HEIGHT;
    } else {
        frame.origin.y += self.view.frame.size.height - self.ldapContainer.frame.size.height - KEYBOARD_HEIGHT;
    }
    
    [UIView animateWithDuration:0.2 animations:^{
        self.view.frame = frame;
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self setup];
    [self adjustStatusBtn];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (IBAction)ldapSwitched:(id)sender {
    [ConfigurationManager sharedInstance].currentLDAP.isUsingLDAP = self.ldapSwitch.on;
    [[ConfigurationManager sharedInstance] saveConfiguration];
    [self toggleLDAPContainer];
}

- (void)toggleLDAPContainer
{
    [self.view layoutIfNeeded];
    [UIView animateWithDuration:0.2 animations:^{
        
        if ([ConfigurationManager sharedInstance].currentLDAP.isUsingLDAP) {
            self.credentialsHeight.constant = LDAP_CONTAINER_HEIGHT;
        } else {
            self.credentialsHeight.constant = 0;
        }
        
        [self.view layoutIfNeeded];
    }];
}

- (void)adjustStatusBtn
{
    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        [self.userStatusBtn setTitle:@"Sign out" forState:UIControlStateNormal];
        [self.ldapSwitch setEnabled:YES];
        [self.offlineSwitch setEnabled:YES];
    } else {
        [self.userStatusBtn setTitle:@"Sign in" forState:UIControlStateNormal];
        [self.ldapSwitch setEnabled:NO];
        [self.offlineSwitch setEnabled:NO];
    }
}

- (BOOL)willKeyboardOverlap
{
    CGFloat heightDiff = self.view.frame.size.height - KEYBOARD_HEIGHT + GENERAL_PADDING;
    CGFloat containerBottom = LDAP_CONTAINER_HEIGHT + self.ldapContainer.frame.origin.y;
    
    return containerBottom >= heightDiff;
}

- (IBAction)offlineSwitched:(id)sender {
    [ConfigurationManager sharedInstance].currentLDAP.isUsingLDAP = self.offlineSwitch.on;
    [[ConfigurationManager sharedInstance] saveConfiguration];
}

- (IBAction)userStatusPressed:(id)sender {
    if ([[UserManager sharedInstance] userHasActiveCredentials] == NO) {
        [self performSegueWithIdentifier:@"presentLogin" sender:self];
    } else {
        [[UserManager sharedInstance] deleteUserCredentials];
        [ConfigurationManager sharedInstance].currentLDAP = [LDAPUser new];
        [self setup];
    }
    
    [self adjustStatusBtn];
}

#pragma mark - Textfield methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!keyboardIsOn && isOverlapping) {
        [self adjustFrame];
    }
    
    keyboardIsOn = YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    LDAPUser *ldapUser = [LDAPUser new];
    ldapUser.LDAPUsername = self.ldapUsername.text;
    ldapUser.LDAPPassword = self.ldapPassword.text;
    ldapUser.isUsingLDAP = YES;
    
    [ConfigurationManager sharedInstance].currentLDAP = ldapUser;
    [[ConfigurationManager sharedInstance] saveConfiguration];
}

#pragma mark - Touch methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
    if (keyboardIsOn && isOverlapping) {
        [self adjustFrame];
    }
    keyboardIsOn = NO;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentLogin"]) {
        
    }
}

@end
