//
//  DatePickerViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 28/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "DatePickerViewController.h"
#import "CustomDatePicker+Date.h"

@interface DatePickerViewController () {
    Type pickerType;
}

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSDate *minDate;

@end

NSString* singlePickerNib = @"DatePicker";
NSString* doublePickerNib = @"DoubleDatePicker";

@implementation DatePickerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil pickerType:(NSInteger)type selectedDate:(NSDate*)date minDate:(NSDate*)mDate
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) {
        
        // Custom initialization
        pickerType = type;
        [self setupDates:date:mDate];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setup];
}

- (void)setup
{
    self.datePicker.date = self.date;
    self.datePicker.minDate = self.minDate;
    
    if ([self.datePicker.date compare:self.datePicker.minDate] == NSOrderedAscending) {
        NSLog(@"Pref date is before minimum date, selecting minimum date as prefdate...");
        self.datePicker.date = self.datePicker.minDate;
    }
    
    self.datePicker.pickerDelegate = self;
    self.datePicker.dateFormat = @"d MMM YYYY";
    self.datePicker.pickerType = pickerType;
    
    if ([self.nibName isEqualToString:doublePickerNib]) {
        
        self.datePickerEnd.pickerDelegate = self;
        self.datePickerEnd.dateFormat = @"d MMM YYYY";
        
        self.datePickerEnd.date = self.date;
        self.datePickerEnd.minDate = self.minDate;
        self.datePickerEnd.pickerType = pickerType;
        
        [self.datePickerEnd reloadData];
    }
    
    [self.datePicker reloadData];
}

- (void)setupDates:(NSDate *)date :(NSDate*)mDate
{
    if (pickerType == DateAndTimeType) {
        self.minDate = mDate;
    } else {
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:mDate];
        [components setHour:0];
        self.minDate = [calendar dateFromComponents:components];
    }
    
    self.date = date;
}

- (IBAction)doneButtonPressed:(id)sender
{
    if ([self.datePickerDelegate respondsToSelector:@selector(userDidPickDate:)]) {
        [self.datePickerDelegate userDidPickDate:self.datePicker.date];
    }
    
    if ([self.datePickerDelegate respondsToSelector:@selector(userDidPickDates:)]) {
        NSArray *dates = @[self.datePicker.date, self.datePickerEnd.date];
        [self.datePickerDelegate userDidPickDates:dates];
    }
}

- (IBAction)cancelButtonPressed:(id)sender
{
    if (self.datePickerDelegate) {
        [self.datePickerDelegate userDidCancel];
    }
}

- (void)dateChanged:(id)sender withRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (self.datePickerEnd) {
        
        if (sender == self.datePickerEnd) {
            
            if ([self.datePicker.date compare:self.datePickerEnd.date] == NSOrderedDescending) {
                [self.datePickerEnd scrollAtRowInComponent:[self.datePicker getPickerSelectedRow] component:component];
                self.datePickerEnd.date = self.datePicker.date;
            }
            
        } else {
                [self.datePickerEnd scrollAtRowInComponent:row component:component];
                self.datePickerEnd.date = self.datePicker.date;
        }
        
    } else {
        
        if ([self.datePickerDelegate respondsToSelector:@selector(handleDateChange:)]) {
            [self.datePickerDelegate handleDateChange:self.datePicker.date];
        }
        
    }
}

@end
