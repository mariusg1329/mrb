//
//  AllAppointmentsViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "AllAppointmentsViewController.h"
#import "AppointmentViewController.h"
#import "BookingsManager.h"
#import "Location.h"
#import "Booking.h"
#import "CustomDatePicker.h"
#import "RoomPickerViewController.h"
#import "FilterManager.h"

@interface AllAppointmentsViewController () {
    BOOL isPickerVisible;
    DatePickerViewController *dpVC;
    BOOL alertIsShowing;
}

@property (weak, nonatomic) IBOutlet UITableView *bookingsTable;
@property (weak, nonatomic) IBOutlet UILabel *dateIntervalLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomPickerLabel;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortControl;
@property (strong, nonatomic) NSMutableArray *selectedDates;
@property (weak, nonatomic) NSIndexPath *selectedIndex;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UILabel *emptyLabel;

@end

#define PICKER_Y_PADDING 10

@implementation AllAppointmentsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    
    self.bookingsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.sortControl.selectedSegmentIndex = 0;
    
    if ([BookingsManager sharedInstance].bookings.count == 0) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [self refresh];
    }
    
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([BookingsManager sharedInstance].listNeedsUpdate) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] getWSBookings];
    }
    
    if ([BookingsManager sharedInstance].bookings.count > 0) {
        [self selectionChanged:self.sortControl];
        [self.emptyLabel setHidden:YES];
    } else {
        [self.emptyLabel setHidden:NO];
    }
    
    if ([FilterManager instance].roomFilterArray.count > 1) {
        self.roomPickerLabel.text = @"Multiple";
    } else if ([FilterManager instance].roomFilterArray.count == 1){
        NSString *locName = [[FilterManager instance].roomFilterArray objectAtIndex:0];
        self.roomPickerLabel.text = locName;
    } else {
        self.roomPickerLabel.text = @"Tap to select";
    }

    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.refreshControl endRefreshing];
    [BookingsManager sharedInstance].bookingsDelegate = nil;
    
    if (isPickerVisible) {
        [self toggleDatePicker];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [BookingsManager sharedInstance].bookingsDelegate = self;
}

#pragma mark - Private methods

- (void)setup
{
    self.emptyLabel.text = ALL_APPOINTMENTS_EMTPY;
    [self.emptyLabel sizeToFit];
    
    if ([BookingsManager sharedInstance].listNeedsUpdate == YES) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] getWSBookings];
    }
    
    self.selectedDates = [[NSMutableArray alloc] init];
    
    //Initialize datasource
    [[BookingsManager sharedInstance] getBookingsGroupedByDate];
    [[BookingsManager sharedInstance] getBookingsGroupedByLocation];
    
    //First display
    [BookingsManager sharedInstance].currentBookings = [BookingsManager sharedInstance].bookingsByLocation;
    
    //Initialize and setup DatePicker
    dpVC = [[DatePickerViewController alloc] initWithNibName:@"DoubleDatePicker" bundle:[NSBundle mainBundle] pickerType:DateType selectedDate:[NSDate date] minDate:[[NSDate alloc] initWithTimeIntervalSinceNow:(NSTimeInterval) -(3600*24*200)]];
    dpVC.datePickerDelegate = self;

    self.dateIntervalLabel.userInteractionEnabled = YES;
    
    CGRect pickerFrame = dpVC.view.frame;
    pickerFrame.origin.y = self.tabBarController.view.frame.size.height + pickerFrame.size.height;
    dpVC.view.frame = pickerFrame;
    [self.tabBarController.view addSubview:dpVC.view];
    
    dpVC.datePickerEnd.minDate = dpVC.datePicker.minDate;
    isPickerVisible = NO;
    
    //RoomPicker setup
    self.roomPickerLabel.userInteractionEnabled = YES;
    
    //Add refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.bookingsTable addSubview:self.refreshControl];
}

- (void)refresh
{
    [[BookingsManager sharedInstance] getWSBookings];
}

- (void)toggleDatePicker
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect pickerFrame = dpVC.view.frame;
        
        if (isPickerVisible) {
            pickerFrame.origin.y = self.tabBarController.view.frame.size.height + pickerFrame.size.height;
            isPickerVisible = NO;
            
        } else {
            pickerFrame.origin.y = self.tabBarController.view.frame.size.height - pickerFrame.size.height + PICKER_Y_PADDING;
            isPickerVisible = YES;
        }
        
        dpVC.view.frame = pickerFrame;
    }];
    
}

- (IBAction)clearDatesFilter:(id)sender {
    if ([FilterManager instance].dateFilterArray.count > 0) {
        [[FilterManager instance].dateFilterArray removeAllObjects];
        [self selectionChanged:self.sortControl];
        self.dateIntervalLabel.text = @"Tap to select";
    }
}

- (IBAction)clearRoomFilter:(id)sender {
    if ([FilterManager instance].roomFilterArray.count > 0) {
        [[FilterManager instance].roomFilterArray removeAllObjects];
        [self selectionChanged:self.sortControl];
        self.roomPickerLabel.text = @"Tap to select";
    }
}

- (IBAction)selectionChanged:(id)sender {
    
    UISegmentedControl *segmentedControl = sender;
    
    if (segmentedControl.selectedSegmentIndex == 0) {
        [[BookingsManager sharedInstance] getBookingsGroupedByLocation];
        [BookingsManager sharedInstance].currentBookings = [BookingsManager sharedInstance].bookingsByLocation;
    } else {
        [[BookingsManager sharedInstance] getBookingsGroupedByDate];
        [BookingsManager sharedInstance].currentBookings = [BookingsManager sharedInstance].bookingsByDates;
    }
    
    [[FilterManager instance] applyFilters];
    [GeneralUtils reloadTable:self.bookingsTable animated:YES];
}

#pragma mark - DatePicker delegate methods

- (void)userDidPickDates:(NSArray *)dates
{
    self.dateIntervalLabel.text = [NSString stringWithFormat:@"%@\n%@",[GeneralUtils daySuffixForDate:[dates objectAtIndex:0]], [GeneralUtils daySuffixForDate:[dates objectAtIndex:1]]];
    
    [FilterManager instance].dateFilterArray = [NSMutableArray arrayWithArray:dates];
    
    [self selectionChanged:self.sortControl];
    [self toggleDatePicker];
}

- (void)userDidCancel
{
    [self toggleDatePicker];
}

#pragma mark - Touch delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches allObjects].lastObject;
    
    if (touch.view == self.roomPickerLabel) {
        [LocationsManager sharedInstance].selectedLocations = [FilterManager instance].roomFilterArray;
        [self performSegueWithIdentifier:@"presentRoomList" sender:self];
    } else if (!isPickerVisible && touch.view == self.dateIntervalLabel) {
        [self toggleDatePicker];
    }
}

#pragma mark - UITableView delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    id titleKey = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:indexPath.section];
    Booking *booking = [[[BookingsManager sharedInstance].currentBookings objectForKey:titleKey] objectAtIndex:indexPath.row];
    
    if (booking.isApproved == NO) {
        return;
    }
    
    self.selectedIndex = indexPath;
    [self performSegueWithIdentifier:@"presentAppointmentDetails" sender:self];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    id titleKey = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:indexPath.section];
    Booking *booking = [[[BookingsManager sharedInstance].currentBookings objectForKey:titleKey] objectAtIndex:indexPath.row];
    
    if (booking.isApproved == NO) {
        return NO;
    }
    
    return [booking.organizer isEqualToString:[[UserManager sharedInstance] getActiveUser].userName];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDate *key = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:indexPath.section];
        NSMutableArray *values = [[BookingsManager sharedInstance].currentBookings objectForKey:key];

        Booking *b = [values objectAtIndex:indexPath.row];
        
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] cancelBooking:b];
        [tableView setEditing:NO animated:YES];
    }
}

#pragma mark - UITableView datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[BookingsManager sharedInstance].currentBookings allKeys].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:section];
    NSArray *bookingsCurrentLocation = [[BookingsManager sharedInstance].currentBookings objectForKey:key];
    
    return bookingsCurrentLocation.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Bookings";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDate *titleKey = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:indexPath.section];
    Booking *booking = [[[BookingsManager sharedInstance].currentBookings objectForKey:titleKey] objectAtIndex:indexPath.row];
    
    UILabel *titleLabel = (UILabel*)[cell viewWithTag:1];
    UILabel *detailLabel = (UILabel*)[cell viewWithTag:2];
    UILabel *subjectLabel = (UILabel *)[cell viewWithTag:3];
    UIImageView *editImage = (UIImageView *)[cell viewWithTag:4];
    UILabel *statusView = (UILabel *)[cell viewWithTag:5];
    
    subjectLabel.text = booking.title;

    if ([booking.organizer isEqualToString:[[UserManager sharedInstance] getActiveUser].userName]) {
        editImage.hidden = NO;
    } else {
        editImage.hidden = YES;
    }
    
    if (booking.isApproved == NO) {
        if (booking.isBeingCanceled) {
            statusView.backgroundColor = [UIColor redColor];
            statusView.text = @"Cancelled";
        } else {
            statusView.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
            statusView.text = @"Added";
        }
        
    } else {
        statusView.backgroundColor = [UIColor clearColor];
        statusView.text = @"";
    }
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    
    detailLabel.text = [NSString stringWithFormat:@"%@ > %@", [timeFormatter stringFromDate:booking.startDate], [timeFormatter stringFromDate:booking.endDate]];
    
    //In case an appointment lasts till next day, lol
    if (![GeneralUtils compareDate:booking.startDate toDate:booking.endDate]) {
        detailLabel.text = [detailLabel.text stringByAppendingString:@"+"];
    }
    
    switch (self.sortControl.selectedSegmentIndex) {
        case 0:
            titleLabel.text = [NSString stringWithFormat:@"%@", [GeneralUtils daySuffixForDate:booking.startDate]];
            break;
        case 1:
            titleLabel.text = [NSString stringWithFormat:@"%@ \"%@\", %@ floor",booking.location.name , booking.location.size, [GeneralUtils counterFromInt:booking.location.floor + 1]];
            break;
        default:
            break;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, HEADER_HEIGHT)];
    header.backgroundColor = tableView.backgroundColor;
    
    header.textAlignment = NSTextAlignmentCenter;
    header.numberOfLines = 1;
    header.textColor = [UIColor orangeColor];
    
    id title = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:section];
    Booking *booking = [[[BookingsManager sharedInstance].currentBookings objectForKey:title] objectAtIndex:0];
    
    NSString *titleString;
    
    switch (self.sortControl.selectedSegmentIndex) {
        case 0:
             titleString = [NSString stringWithFormat:@"%@ \"%@\", %@ floor",title, booking.location.size, [GeneralUtils counterFromInt:booking.location.floor + 1]];
            break;
        case 1:
            titleString = [GeneralUtils daySuffixForDate:booking.startDate];
            break;
        default:
            titleString = [NSString stringWithFormat:@"%@ \"%@\", %@ floor",title, booking.location.size, [GeneralUtils counterFromInt:booking.location.floor + 1]];
            break;
    }
    
    header.text = titleString;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

#pragma mark - WS Response

- (void)uploadFinished:(Booking *)booking
{
    [[BookingsManager sharedInstance] getWSBookings];
}

- (void)bookingsRetrieved
{
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    [GeneralUtils hideLoadingOn:self.tabBarController];
    
    if ([BookingsManager sharedInstance].bookings.count > 0) {
        [self selectionChanged:self.sortControl];
        [self.emptyLabel removeFromSuperview];
    } else {
        [self.view addSubview:self.emptyLabel];
    }
}

- (void)wsRequestError:(NSError *)error
{
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }

    [GeneralUtils hideLoadingOn:self.tabBarController];
    
    if (alertIsShowing == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
        [alert show];
        alertIsShowing = YES;
    }
}

#pragma mark - AlertView methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retry"]) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] getWSBookings];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    alertIsShowing = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"presentRoomList"]) {
        [segue.destinationViewController setIsMultiPicker:YES];
    } else if ([segue.identifier isEqualToString:@"presentAppointmentDetails"]) {
        NSDate *titleKey = [[[BookingsManager sharedInstance].currentBookings allKeys] objectAtIndex:self.selectedIndex.section];
        Booking *booking = [[[BookingsManager sharedInstance].currentBookings objectForKey:titleKey] objectAtIndex:self.selectedIndex.row];

        if ([booking.organizer isEqualToString:[[UserManager sharedInstance] getActiveUser].userName]) {
            [segue.destinationViewController setEditingEnabled:YES];
        } else {
            [segue.destinationViewController setEditingEnabled:NO];
        }
        
        [segue.destinationViewController setCurrentBooking:booking];
    }
}


@end
