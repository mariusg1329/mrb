//
//  LegendViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "LegendViewController.h"
#import "LocationsManager.h"
#import "Location.h"

@interface LegendViewController ()

@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) IBOutlet UITableView *tableView;

@end

#define CELL_LEFT_PADDING 15

@implementation LegendViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    
    self.descriptionLabel.text = NSLocalizedString(@"legend.description.text", nil);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    // Group locations by floor
    [[LocationsManager sharedInstance] getGroupedLocations];
    
    [GeneralUtils reloadTable:self.tableView animated:NO];
}

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - TableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *locations = [[LocationsManager sharedInstance].groupedLocations objectForKey:[NSNumber numberWithInteger:section]];
    return locations.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[LocationsManager sharedInstance].groupedLocations allKeys].count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSArray *locationsForSection = [[LocationsManager sharedInstance].groupedLocations objectForKey:[NSNumber numberWithInteger:indexPath.section]];
    
    Location *location = [locationsForSection objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@, %@",location.name,location.size];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:15.0f];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, HEADER_HEIGHT)];
    
    headerView.backgroundColor = tableView.backgroundColor;
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(CELL_LEFT_PADDING, 0, headerView.frame.size.width - CELL_LEFT_PADDING, headerView.frame.size.height)];
    
    BuildingFloor floor = section;
    
    switch (floor) {
        case GroundFloor:
            textLabel.text = [NSString stringWithFormat:@"%@ Floor - Rivers floor", [GeneralUtils counterFromInt:floor + 1]];
            break;
        case FirstFloor:
            textLabel.text = [NSString stringWithFormat:@"%@ Floor - Mountain Tops floor", [GeneralUtils counterFromInt:floor + 1]];
            break;
        case SecondFloor:
            textLabel.text = [NSString stringWithFormat:@"%@ Floor - Cities floor", [GeneralUtils counterFromInt:floor + 1]];
            break;
        case ThirdFloor:
            textLabel.text = [NSString stringWithFormat:@"%@ Floor - Museums floor", [GeneralUtils counterFromInt:floor + 1]];

            break;
        case FourthFloor:
            textLabel.text = [NSString stringWithFormat:@"%@ Floor - ", [GeneralUtils counterFromInt:floor + 1]];
            break;
        default:
            break;
    }
    
    textLabel.textColor = [UIColor orangeColor];
    textLabel.backgroundColor = [UIColor clearColor];
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.frame.size.height - 1, headerView.frame.size.width, 1)];
    separator.backgroundColor = tableView.separatorColor;
    
    [headerView addSubview:separator];
    [headerView addSubview:textLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

@end
