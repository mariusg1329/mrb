//
//  MyAppointmentsViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "MyAppointmentsViewController.h"
#import "LoginViewController.h"
#import "BookingsManager.h"
#import "Booking.h"
#import "LocationsManager.h"
#import "DataParser.h"
#import "AppointmentViewController.h"
#import "WSBookingManager.h"
#import "UserManager.h"
#import "User.h"
#import "NSMutableArray+Bookings.h"


@interface MyAppointmentsViewController () {
    BOOL alertIsShowing;
}

@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) Booking *currentBooking;
@property (strong, nonatomic) NSString *user;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UILabel *emptyLabel;
@property (strong, nonatomic) IBOutlet UIButton *signInBtn;

@end

@implementation MyAppointmentsViewController

#pragma mark - Private methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    
    // For iOS 6
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back"
                                      style:UIBarButtonItemStylePlain
                                     target:nil
                                     action:nil];

    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] getWSBookings];
    }
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated
{
    [BookingsManager sharedInstance].bookingsDelegate = self;
    
    if ([BookingsManager sharedInstance].listNeedsUpdate == YES) {
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] getWSBookings];
    }
    
    if ([[UserManager sharedInstance] userHasActiveCredentials] == NO) {
        [[BookingsManager sharedInstance].myBookings removeAllObjects];
        self.navigationItem.title = [NSString stringWithFormat:@"My appointments"];
        [self.refreshControl removeFromSuperview];
        
        self.emptyLabel.text = SIGN_IN_MY_APPOINTMENTS;
        [self.emptyLabel sizeToFit];
        
        [self.emptyLabel setHidden:NO];
        [self.signInBtn setHidden:NO];
        [self.addBtn setHidden:YES];
    } else {
        User *user = [[UserManager sharedInstance] getActiveUser];
        NSString *userName = user.userName;
        userName = [userName stringByReplacingOccurrencesOfString:ZIMBRA_EMAIL_TAG withString:@""];
        self.navigationItem.title = [NSString stringWithFormat:@"Appointments for %@", userName];
        
        if ([BookingsManager sharedInstance].myBookings.count > 0) {
            [self.emptyLabel setHidden:YES];
        } else {
            [self.emptyLabel setHidden:NO];
        }
        
        self.emptyLabel.text = MY_APPOINTMENTS_EMPTY;
        [self.emptyLabel sizeToFit];
        [self.tableView addSubview:self.refreshControl];
        
        [self.addBtn setHidden:NO];
        [self.signInBtn setHidden:YES];
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [GeneralUtils reloadTable:self.tableView animated:YES];
    
    alertIsShowing = NO;
}

- (void)viewDidAppear:(BOOL)animated
{    
    [BookingsManager sharedInstance].bookingsDelegate = self;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [BookingsManager sharedInstance].bookingsDelegate = nil;
}

- (void)viewDidDisappear:(BOOL)animated
{
    [self.refreshControl endRefreshing];
}

- (void)setup
{
    [BookingsManager sharedInstance].bookingsDelegate = self;
    
    //Refresh control
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    
    [self.tableView addSubview:self.refreshControl];
}

- (void)refresh
{
    [[BookingsManager sharedInstance] getWSBookings];
}

- (IBAction)signInPressed:(id)sender
{
    [self performSegueWithIdentifier:@"presentLogin" sender:self];
}

#pragma mark - TableView delegate methods

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDate *key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:indexPath.section];
    NSMutableArray *values = [[BookingsManager sharedInstance].myBookings objectForKey:key];
    Booking *b = [values objectAtIndex:indexPath.row];
    
    if (b.isApproved == NO) {
        return NO;
    }
    
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSDate *key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:indexPath.section];
        NSMutableArray *values = [[BookingsManager sharedInstance].myBookings objectForKey:key];
        Booking *b = [values objectAtIndex:indexPath.row];
        
        [GeneralUtils showLoadingOn:self.tabBarController];
        [[BookingsManager sharedInstance] cancelBooking:b];
        [tableView setEditing:NO animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[BookingsManager sharedInstance].myBookings allKeys].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDate *key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:section];
    NSMutableArray *values = [[BookingsManager sharedInstance].myBookings objectForKey:key];
    
    return values.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Appointment";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDate *key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:indexPath.section];
    NSMutableArray *values = [[BookingsManager sharedInstance].myBookings objectForKey:key];
    Booking *booking = [values objectAtIndex:indexPath.row];
    
    UILabel *locationLabel, *durationLabel, *subjectLabel, *statusView;
    
    locationLabel = (UILabel*)[cell viewWithTag:3];
    durationLabel = (UILabel*)[cell viewWithTag:2];
    subjectLabel = (UILabel *)[cell viewWithTag:1];
    statusView = (UILabel *)[cell viewWithTag:4];
    
    if (booking.isApproved == NO) {
        if (booking.isBeingCanceled) {
            statusView.backgroundColor = [UIColor redColor];
            statusView.text = @"Cancelled";
        } else {
            statusView.backgroundColor = [UIColor colorWithRed:0 green:1 blue:0 alpha:1];
            statusView.text = @"Added";
        }
        
    } else {
        statusView.backgroundColor = [UIColor clearColor];
        statusView.text = @"";
    }
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    
    locationLabel.text = [NSString stringWithFormat:@"%@ \"%@\", floor %u",booking.location.name, booking.location.size, (unsigned int)booking.location.floor + 1];
    durationLabel.text = [NSString stringWithFormat:@"%@ > %@", [timeFormatter stringFromDate:booking.startDate], [timeFormatter stringFromDate:booking.endDate]];
    subjectLabel.text = booking.title;
    
    return cell;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, CELL_HEIGHT)];
    label.numberOfLines = 1;
    label.font = [UIFont systemFontOfSize:15];
    

    id key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:section];
    
    if ([key isKindOfClass:[NSDate class]]) {
        label.text = [GeneralUtils daySuffixForDate:key];
    } else {
        label.text = key;
    }
    
    label.textColor = [UIColor orangeColor];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = tableView.backgroundColor;
    
    return label;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HEADER_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDate *key = [[[BookingsManager sharedInstance].myBookings allKeys] objectAtIndex:indexPath.section];
    NSMutableArray *values = [[BookingsManager sharedInstance].myBookings objectForKey:key];
    self.currentBooking = [values objectAtIndex:indexPath.row];

    if (self.currentBooking.isApproved) {
        [self performSegueWithIdentifier:@"presentAppointmentDetails" sender:self];
    }
    
}

#pragma mark - WS bookings delegate

- (void)bookingsRetrieved
{
    [GeneralUtils hideLoadingOn:self.tabBarController];
    
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    if ([BookingsManager sharedInstance].myBookings.count == 0) {
        [self.emptyLabel setHidden:NO];
    } else {
        [self.emptyLabel setHidden:YES];
    }
    
    [GeneralUtils reloadTable:self.tableView animated:YES];
}

- (void)uploadFinished:(Booking *)booking
{
    [GeneralUtils reloadTable:self.tableView animated:YES];
    [[BookingsManager sharedInstance] getWSBookings];
}

- (void)getAllBookings
{
    [GeneralUtils showLoadingOn:self.tabBarController];
    [[BookingsManager sharedInstance] getWSBookings];
}

- (void)wsRequestError:(NSError *)error
{
    if ([self.refreshControl isRefreshing]) {
        [self.refreshControl endRefreshing];
    }
    
    [GeneralUtils hideLoadingOn:self.tabBarController];
    
    if (alertIsShowing == NO) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:@"Retry", nil];
        [alert show];
        alertIsShowing = YES;
    }
}

#pragma mark - AlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Sign In"]) {
        [self performSegueWithIdentifier:@"presentLogin" sender:self];
    } else if ([[alertView buttonTitleAtIndex:buttonIndex] isEqualToString:@"Retry"]) {
        [self getAllBookings];
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    alertIsShowing = NO;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentLogin"]) {
        
    } else if ([segue.identifier isEqualToString:@"presentAppointmentDetails"]) {
        [segue.destinationViewController setCurrentBooking:self.currentBooking];
        [segue.destinationViewController setEditingEnabled:YES];
    } else if ([segue.identifier isEqualToString:@"presentAddAppointment"]) {
        [segue.destinationViewController setTitle:self.user];
        [segue.destinationViewController setIsAdding:YES];
    }
}

@end
