//
//  LoginViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "LoginViewController.h"
#import "MyAppointmentsViewController.h"
#import "BookingsManager.h"
#import "LocationsManager.h"
#import "Location.h"
#import "Booking.h"
#import "User.h"
#import "WSBookingManager.h"
#import "UserManager+Cache.h"
#import "ConfigurationManager.h"

@interface LoginViewController () {
    BOOL keyboardIsOn;
    IBOutlet NSLayoutConstraint *offsetY;
}

@property (nonatomic, strong) IBOutlet UITextField *userName;
@property (nonatomic, strong) IBOutlet UITextField *password;
@property (nonatomic, strong) IBOutlet UIButton *loginBtn;
@property (nonatomic, weak) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *cancelView;

@end

@implementation LoginViewController

static float topSpace = 263;

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.userName.delegate = self;
    self.password.delegate = self;
    
    keyboardIsOn = NO;
    
    offsetY.constant = topSpace;
    
    [[UserManager sharedInstance] deleteUserCredentials];
    [BookingsManager sharedInstance].signInDelegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

#pragma mark - Private methods

- (IBAction)loginPressed:(id)sender {
    keyboardIsOn = NO;
    [self.view endEditing:YES];

    if ([self fieldsAreValid]) {

        User *user = [[User alloc] init];
        user.userName = self.userName.text;
        user.password = self.password.text;
        
        [GeneralUtils showLoadingOn:self];

        [self proceedWithSignIn:user];
        
    } else {
        [self showFailureAlert:@"Invalid input"];
        [self clearFields];
        
        //handle dismiss keyboard
        [self setLoginOffset:NO];
    }
}

- (void)showFailureAlert:(NSString *)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"Retry" otherButtonTitles:nil];
    [alert show];
}

- (void)proceedWithSignIn:(User *)user
{
    [[UserManager sharedInstance] saveUserCredentials:user];
    [GeneralUtils showLoadingOn:self];
    [[BookingsManager sharedInstance] checkUserItems];
}

- (void)setLoginOffset:(BOOL)keyboardStatus
{
    if (keyboardStatus) {
        offsetY.constant = self.view.frame.size.height - KEYBOARD_HEIGHT - self.containerView.frame.size.height;
    } else {
        offsetY.constant = topSpace;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        [self.view layoutSubviews];
    }];
}

- (void)clearFields
{
    self.password.text = @"";
}

- (BOOL)fieldsAreValid
{
    if ([self.userName.text isEqualToString:@""]) {
        return NO;
    }
    
    if ([self.password.text isEqualToString:@""]) {
        return NO;
    }
    
    return YES;
}

#pragma mark - Touch handling methods

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches allObjects].lastObject;
    
    if (touch.view == self.cancelView) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
    [self.view endEditing:YES];
    keyboardIsOn = NO;
    [self setLoginOffset:keyboardIsOn];
}

#pragma mark - UITextfield delegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (!keyboardIsOn) {
        keyboardIsOn = YES;
        [self setLoginOffset:keyboardIsOn];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (!keyboardIsOn) {
        [self setLoginOffset:keyboardIsOn];
    }
}

#pragma mark - Sign in methods

- (void)signInSuccessful
{
    [GeneralUtils hideLoadingOn:self];
    [[ConfigurationManager sharedInstance] loadConfiguration];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)wsRequestError:(NSError *)error
{
    [GeneralUtils hideLoadingOn:self];
    [self setLoginOffset:NO];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Login error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:self cancelButtonTitle:@"Retry" otherButtonTitles: nil];
    [alert show];
    
    [self clearFields];
    [[UserManager sharedInstance] deleteUserCredentials];
}

@end
