//
//  LegendViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LegendViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@end
