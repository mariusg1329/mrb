//
//  RoomPickerViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 25/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Location;


@protocol RoomPickerDelegate <NSObject>

- (void)didPickRoomWithLocation:(Location*)location;

@end


@interface RoomPickerViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<RoomPickerDelegate> roomPickerDelegate;
@property (nonatomic) BOOL isMultiPicker;

@end
