//
//  EditableAppointmentViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BookingsManager.h"
@class Booking;
#import "RoomPickerViewController.h"
#import "DatePickerViewController.h"

@protocol EditableAppointmentDelegate <NSObject>

@required
- (void)userDidEditBooking:(Booking*)booking;

@optional
- (void)userDidAddBooking:(Booking*)booking;

@end

@interface EditableAppointmentViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, RoomPickerDelegate, UITextViewDelegate, DatePickerDelegate, UIGestureRecognizerDelegate, BookingsDelegate, UISearchBarDelegate>

@property (nonatomic) BOOL isAdding;
@property (nonatomic, strong) Booking* bookingToEdit;

@property (nonatomic, weak) id<EditableAppointmentDelegate> editDelegate;

@end
