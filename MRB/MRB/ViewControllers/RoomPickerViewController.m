//
//  RoomPickerViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 25/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "RoomPickerViewController.h"
#import "LocationsManager.h"
#import "Location.h"
#import "BookingsManager.h"
#import "FilterManager.h"
#import "RoomSelectionCell.h"

@interface RoomPickerViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;

@end

@implementation RoomPickerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    
    if (self.isMultiPicker) {
        [self.saveBtn setHidden:NO];
        [self.saveBtn setEnabled:YES];
        [self.tableView setAllowsMultipleSelection:YES];
    }
}

- (IBAction)savePressed:(id)sender
{    
    [FilterManager instance].roomFilterArray = [LocationsManager sharedInstance].selectedLocations;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)cancelPressed:(id)sender {
//    [[LocationsManager sharedInstance].selectedLocations removeAllObjects];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view delegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (self.isMultiPicker) {
        
        NSString *locationName = [[[LocationsManager sharedInstance].locations objectAtIndex:indexPath.row] name];
        
        if (![[LocationsManager sharedInstance].selectedLocations containsObject:locationName]) {
            [[LocationsManager sharedInstance].selectedLocations addObject:locationName];
        }
        
    } else {
        if ([self.roomPickerDelegate respondsToSelector:@selector(didPickRoomWithLocation:)]) {
            [self.roomPickerDelegate didPickRoomWithLocation:[[LocationsManager sharedInstance].locations objectAtIndex:indexPath.row]];
        }
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *locationName = [[[LocationsManager sharedInstance].locations objectAtIndex:indexPath.row] name];
    [[LocationsManager sharedInstance].selectedLocations removeObject:locationName];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"roomCell";
    RoomSelectionCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[RoomSelectionCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UIImageView *image = (UIImageView*)[cell viewWithTag:2];
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    UIButton *checkBtn = (UIButton*)[cell viewWithTag:3];
    
    cell.checkButton = checkBtn;
    
    Location *location = [[LocationsManager sharedInstance].locations objectAtIndex:indexPath.row];

    
    if (!self.isMultiPicker) {
        checkBtn.hidden = YES;
        checkBtn.enabled = NO;
    } else {
        checkBtn.enabled = YES;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;

        if ([[LocationsManager sharedInstance].selectedLocations containsObject:location.name]) {
            [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
        }
    }

    if ([location.size isEqualToString:@"M"]) {
        [image setImage:[UIImage imageNamed:@"SmallRoomImage"]];
    } else {
        [image setImage:[UIImage imageNamed:@"LargeRoomImage"]];
    }
    
    label.text = [NSString stringWithFormat:@"%@, %@(floor %u)",location.name, location.size, (unsigned int)location.floor];
    
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [LocationsManager sharedInstance].locations.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 1)];
    [headerView setBackgroundColor:[UIColor lightGrayColor]];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 1;
}

@end
