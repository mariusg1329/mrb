//
//  AppointmentsViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Booking;
#import "EditableAppointmentViewController.h"
#import "BookingsManager.h"

@protocol AppointmentDelegate <NSObject>

@optional

- (void)didDeleteAppointment:(Booking*)booking;
- (void)didEditAppointment:(Booking*)booking;

@end

@interface AppointmentViewController : UIViewController <EditableAppointmentDelegate, BookingsDelegate>

- (void)setEditingEnabled:(BOOL)isEditable;

@property (strong, nonatomic) Booking* currentBooking;
@property (weak, nonatomic) id<AppointmentDelegate> appointmentDelegate;

@end
