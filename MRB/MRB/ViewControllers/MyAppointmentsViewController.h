//
//  MyAppointmentsViewController.h
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "BookingsManager.h"

@interface MyAppointmentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, BookingsDelegate, UIAlertViewDelegate>

@end
