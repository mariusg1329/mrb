//
//  EditableAppointmentViewController.m
//  MRB
//
//  Created by Ghita Marius Lucian on 22/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "EditableAppointmentViewController.h"
#import "Booking.h"
#import "BookingsManager.h"
#import "MyAppointmentsViewController.h"
#import "CustomDatePicker.h"
#import "EmailSearchView.h"
#import "ConfigurationManager.h"
#import "RHLDAPSearch+additions.h"

@interface EditableAppointmentViewController () {
    UILabel *currentDateLabel;
    BOOL isPickerVisible;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeight;
@property (weak, nonatomic) IBOutlet UITableView *attendeesTableView;
@property (weak, nonatomic) IBOutlet UIView *locationSelectView;
@property (weak, nonatomic) IBOutlet UILabel *locationName;
@property (weak, nonatomic) IBOutlet UILabel *startDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *endDateLabel;
@property (weak, nonatomic) IBOutlet UITextView *editDescriptionTextView;
@property (weak, nonatomic) IBOutlet UITextView *subjectTextView;

@property (strong, nonatomic) DatePickerViewController *datePickerVC;
@property (strong, nonatomic) UIView *headerView;

@end

@implementation EditableAppointmentViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]];
    [self setup];
}

- (void)viewWillAppear:(BOOL)animated
{
    [BookingsManager sharedInstance].bookingsDelegate = self;
}

#pragma mark - Search bar delegate methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    EmailSearchView *searchView = (EmailSearchView *)searchBar.superview;
    
    //The search object has to be created every time due to this very lame library
    RHLDAPSearch *search = [[RHLDAPSearch alloc] initWithURL:LDAP_SERVER];
    
    if ([ConfigurationManager sharedInstance].currentLDAP.isUsingLDAP) {
        if (searchText.length > 2) {
            
            if (searchView.spinner.isAnimating == NO) {
                [searchView.spinner startAnimating];
            }
            
            [search searchWithQuery:[NSString stringWithFormat:@"(mail=%@*)",searchBar.text] withinBase:@"ou=People,dc=fortech,dc=ro" usingScope:RH_LDAP_SCOPE_SUBTREE completionBlock:^{
                
                [searchView.spinner performSelector:@selector(stopAnimating) withObject:nil afterDelay:0.7];
                
                if (!search.error) {
                    searchView.dataSource = search.resultArray;
                    [searchView.resultsTable reloadData];
                    searchView.emptyLabel.hidden = YES;
                } else {                    
                    searchView.emptyLabel.hidden = NO;
                    searchView.emptyLabel.text = [NSString stringWithFormat:@"Search result returned error.\n%@",[search.error.userInfo valueForKey:@"err_msg"]];
                }
            }];
        } else if (searchText.length == 0) {
            [searchView.dataSource removeAllObjects];
            [searchView.spinner stopAnimating];
            [searchView.resultsTable reloadData];
            searchView.emptyLabel.hidden = NO;
            searchView.emptyLabel.text = EMAIL_SEARCH_CHARACTERS;
        }
    } else {
        searchView.emptyLabel.hidden = NO;
        searchView.emptyLabel.text = EMAIL_SEARCH_LDAP;
    }    
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.bookingToEdit.attendees addObject:searchBar.text];
    
    EmailSearchView *searchView = (EmailSearchView *)searchBar.superview;
    searchView.shouldEndEditting = YES;
    [searchBar.superview removeFromSuperview];
    
    [self.attendeesTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
{
    EmailSearchView *searchView = (EmailSearchView *)searchBar.superview;
    
    if (searchView.shouldEndEditting == YES) {
        return YES;
    }
    
    return NO;
}

#pragma mark - Room picker delegate methods

- (void)didPickRoomWithLocation:(Location *)location
{
    self.bookingToEdit.location = location;
    self.locationName.text = [NSString stringWithFormat:@"%@ %@", self.bookingToEdit.location.name, self.bookingToEdit.location.size];
}


#pragma mark - Private methods

- (void)setup
{
    self.attendeesTableView.delegate = self;
    self.attendeesTableView.dataSource = self;
    
    self.attendeesTableView.backgroundColor = [UIColor whiteColor];
    
    self.subjectTextView.delegate = self;
    self.editDescriptionTextView.delegate = self;
    
    [self.startDateLabel setTag:5];
    [self.endDateLabel setTag:6];
    
    [self.startDateLabel setUserInteractionEnabled:YES];
    [self.endDateLabel setUserInteractionEnabled:YES];
    [self.view setUserInteractionEnabled:YES];
    
    [self.startDateLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelTapped:)]];
    [self.endDateLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateLabelTapped:)]];
    
//    [[self.startDateLabel.gestureRecognizers objectAtIndex:0] setDelegate:self];
//    [[self.endDateLabel.gestureRecognizers objectAtIndex:0] setDelegate:self];
    
    if (self.isAdding) {
        NSDate *addDate = [NSDate dateWithTimeIntervalSinceNow:600];
        self.datePickerVC = [[DatePickerViewController alloc] initWithNibName:@"DatePicker" bundle:[NSBundle mainBundle] pickerType:DateAndTimeType selectedDate:addDate minDate:addDate];
    } else {
       self.datePickerVC = [[DatePickerViewController alloc] initWithNibName:@"DatePicker" bundle:[NSBundle mainBundle] pickerType:DateAndTimeType selectedDate:[NSDate date] minDate:self.bookingToEdit.startDate];
    }
    
    self.datePickerVC.datePickerDelegate = self;
    CGRect pickerFrame = self.datePickerVC.datePicker.frame;
    pickerFrame.size.width = self.view.frame.size.width;
    pickerFrame.origin.y = self.view.frame.size.height;
    self.datePickerVC.view.frame = pickerFrame;
    
    [self.view addSubview:self.datePickerVC.view];
    isPickerVisible = NO;
    
    [self loadBookingData];
}

- (void)loadBookingData
{
    if (self.isAdding) {
        self.bookingToEdit = [[Booking alloc] init];
    }
    
    self.locationName.text = [NSString stringWithFormat:@"%@ %@", self.bookingToEdit.location.name, self.bookingToEdit.location.size];

    self.startDateLabel.text = [NSString stringWithFormat:@"%@ - %@", [GeneralUtils daySuffixForDate:self.bookingToEdit.startDate], [GeneralUtils timeFromDate:self.bookingToEdit.startDate]];
    self.endDateLabel.text = [NSString stringWithFormat:@"%@ - %@", [GeneralUtils daySuffixForDate:self.bookingToEdit.endDate], [GeneralUtils timeFromDate:self.bookingToEdit.endDate]];
    self.subjectTextView.text = self.bookingToEdit.title;
    self.editDescriptionTextView.text = self.bookingToEdit.description;
}

- (IBAction)cancelPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
    self.isAdding = NO;
}

- (IBAction)addButtonPressed:(id)sender
{
//    if (self.isAdding) {
//        self.bookingToEdit.organizer = [[UserManager sharedInstance] getActiveUser].userName;
//    }
//    
//    [self.bookingToEdit.attendees addObject:self.bookingToEdit.organizer];
//    [self.attendeesTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
    //Attendee search bar with suggestions
    
    [self.view endEditing:YES];
    NSArray *nibs = [[NSBundle mainBundle] loadNibNamed:@"EmailSearchView" owner:self options:nil];
    EmailSearchView *emailSearchView = [nibs objectAtIndex:0];
    emailSearchView.searchBar.delegate = self;
    
    CGRect eFrame = emailSearchView.frame;
    eFrame.size.height = self.view.frame.size.height;
    emailSearchView.frame = eFrame;
    
    [self.view addSubview:emailSearchView];
}

- (IBAction)deleteButtonPressed:(id)sender {
    UIButton *btn = (UIButton*)sender;
    [self.bookingToEdit.attendees removeObjectAtIndex:btn.tag];
    [self.attendeesTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (void)updateBookingData
{
    self.bookingToEdit.title = self.subjectTextView.text;
    self.bookingToEdit.description = self.editDescriptionTextView.text;
}

- (IBAction)savePressed:(id)sender {
    
    [GeneralUtils showLoadingOn:self];
    
    if (self.isAdding) {
        [self updateBookingData];
        [[BookingsManager sharedInstance] uploadBooking:self.bookingToEdit];
    } else {
        self.bookingToEdit.isBeingCanceled = YES;
        self.bookingToEdit.isBeingEdited = YES;
        [self updateBookingData];
        [[BookingsManager sharedInstance] editBooking:self.bookingToEdit];
        
        Booking *cbooking = self.bookingToEdit;
        cbooking.title = self.subjectTextView.text;
        cbooking.description = self.editDescriptionTextView.text;
        [BookingsManager sharedInstance].currentEditedBooking = cbooking;
    }
}

- (IBAction)clearTextView:(id)sender
{
    switch ([sender tag]) {
        case 10:
            self.subjectTextView.text = @"";
            break;
        case 11:
            self.editDescriptionTextView.text = @"";
            break;
        default:
            break;
    }
}

- (void)dateLabelTapped:(id)sender
{
    
    [self toggleDatePicker];
    
}


#pragma mark - DatePicker delegate

- (void)toggleDatePicker
{
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect pickerFrame = self.datePickerVC.datePicker.frame;

        if (isPickerVisible) {
            pickerFrame.origin.y = self.view.frame.size.height + pickerFrame.size.height;
            
            isPickerVisible = NO;

        } else {
            pickerFrame.origin.y = self.view.frame.size.height - pickerFrame.size.height;
            
            isPickerVisible = YES;
        }
        self.datePickerVC.view.frame = pickerFrame;

    }];
    
}

- (void)userDidPickDate:(NSDate *)date
{
    currentDateLabel.text = [NSString stringWithFormat:@"%@ - %@", [GeneralUtils daySuffixForDate:date], [GeneralUtils timeFromDate:date]];
    
    if (currentDateLabel == self.startDateLabel) {
        self.bookingToEdit.startDate = date;
        
        if ([date compare:self.bookingToEdit.endDate] != NSOrderedAscending) {
            self.endDateLabel.text = currentDateLabel.text;
            self.bookingToEdit.endDate = date;
        }
        
    } else if (currentDateLabel == self.endDateLabel) {
        self.bookingToEdit.endDate = date;
        [self.datePickerVC.datePicker scrollToRowWithDate:self.bookingToEdit.startDate];
    }
    
    [self toggleDatePicker];
}

- (void)userDidCancel
{
    [self toggleDatePicker];
}

- (void)handleDateChange:(NSDate *)date
{
    if (currentDateLabel == self.endDateLabel && [date compare:self.bookingToEdit.startDate] == NSOrderedAscending) {
        [self.datePickerVC.datePicker scrollToRowWithDate:self.bookingToEdit.startDate];
    }
}

#pragma mark - Touch delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (touch.view == self.startDateLabel) {
        currentDateLabel = self.startDateLabel;
    } else if (touch.view == self.endDateLabel) {
        currentDateLabel = self.endDateLabel;
    }

    if (!self.isAdding) {
        [self.datePickerVC.datePicker scrollToRowWithDate:self.bookingToEdit.startDate];
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
    
    if ([touches.allObjects.lastObject view] == self.locationSelectView || [touches.allObjects.lastObject view].superview == self.locationSelectView) {
        [self performSegueWithIdentifier:@"presentRoomList" sender:self];
    }
}

#pragma mark - UITextView delegate methods

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView == self.editDescriptionTextView) {
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y -= KEYBOARD_HEIGHT;
        
        [UIView animateWithDuration:0.4 animations:^{
            self.view.frame = viewFrame;
        }];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.editDescriptionTextView) {
        CGRect viewFrame = self.view.frame;
        viewFrame.origin.y += KEYBOARD_HEIGHT;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = viewFrame;
        }];
    }
}

#pragma mark - UITableView delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    [self.view layoutIfNeeded];
    
    self.tableViewHeight.constant = 0;
    
    if (self.bookingToEdit.attendees.count > MAX_DISPLAYED_ATTENDEES) {
        self.tableViewHeight.constant += MAX_DISPLAYED_ATTENDEES * ATTENDEES_CELL_HEIGHT + ATTENDEES_CELL_HEIGHT / 2;

        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    } else if(self.bookingToEdit.attendees.count == 0) {
        self.tableViewHeight.constant = 0;
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
        
    } else {
        self.tableViewHeight.constant += self.bookingToEdit.attendees.count * ATTENDEES_CELL_HEIGHT;
        
        [UIView animateWithDuration:0.2 animations:^{
            [self.view layoutIfNeeded];
        }];
    }
    return self.bookingToEdit.attendees.count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(DEFAULT_LEFT_PADDING, (ATTENDEES_CELL_HEIGHT - DEFAULT_BUTTON_HEIGHT) / 2, DEFAULT_BUTTON_WIDTH, DEFAULT_BUTTON_HEIGHT)];
    UILabel *emailLabel = [[UILabel alloc] initWithFrame:CGRectMake(DEFAULT_LEFT_PADDING + DEFAULT_BUTTON_WIDTH + SPACING, ATTENDEES_CELL_HEIGHT / 2 - DEFAULT_BUTTON_HEIGHT / 2, cell.frame.size.width - btn.frame.size.width - DEFAULT_LEFT_PADDING, DEFAULT_BUTTON_HEIGHT)];
    
    emailLabel.backgroundColor = [UIColor clearColor];
    
    [btn setBackgroundImage:[UIImage imageNamed:@"DeleteButtonImage"] forState:UIControlStateNormal];
    btn.contentMode = UIViewContentModeScaleAspectFit;
    [btn addTarget:self action:@selector(deleteButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    
    [btn setTag:indexPath.row];
    [cell addSubview:btn];
    
    [emailLabel setText:[self.bookingToEdit.attendees objectAtIndex:indexPath.row]];
    [cell addSubview:emailLabel];

    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ATTENDEES_CELL_HEIGHT;
}

#pragma mark - Data validation

- (BOOL)validateFields
{
    return YES;
}

#pragma mark - WS Response

- (void)uploadFinished:(Booking *)booking
{        
    if (self.isAdding && [self.editDelegate respondsToSelector:@selector(userDidAddBooking:)]) {
        [self.editDelegate userDidAddBooking:self.bookingToEdit];
    } else if([self.editDelegate respondsToSelector:@selector(userDidEditBooking:)]) {
        [self.editDelegate userDidEditBooking:self.bookingToEdit];
    }

    self.isAdding = NO;
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)wsRequestError:(NSError *)error
{
    [GeneralUtils hideLoadingOn:self];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error.userInfo valueForKey:@"NSLocalizedDescription"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:@"Retry", nil];
    [alert show];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"presentRoomList"]) {
        [segue.destinationViewController setRoomPickerDelegate:self];
        [segue.destinationViewController setIsMultiPicker:NO];
    }
}

@end
