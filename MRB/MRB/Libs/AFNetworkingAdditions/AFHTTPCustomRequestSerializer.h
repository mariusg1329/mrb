//
//  AFHTTPCustomRequestSerializer.h
//  Circular
//
//  Created by Cosmin Stirbu on 5/26/14.
//  Copyright (c) 2014 MyWebGrocer. All rights reserved.
//

#import "AFURLRequestSerialization.h"

/**
 *  This class is used to set the content body for a request. Right it creates the request it sets its body to nil
 */

@interface AFHTTPCustomRequestSerializer : AFHTTPRequestSerializer

// HTTP Body
@property (nonatomic, strong) NSString *httpBody;

@end
