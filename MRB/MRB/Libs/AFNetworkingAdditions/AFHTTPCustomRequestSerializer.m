//
//  AFHTTPCustomRequestSerializer.m
//  Circular
//
//  Created by Cosmin Stirbu on 5/26/14.
//  Copyright (c) 2014 MyWebGrocer. All rights reserved.
//

#import "AFHTTPCustomRequestSerializer.h"

@implementation AFHTTPCustomRequestSerializer

#pragma mark - AFURLRequestSerialization

- (NSURLRequest *)requestBySerializingRequest:(NSURLRequest *)request withParameters:(id)parameters error:(NSError *__autoreleasing *)error {
    if (self.httpBody != nil) {
        NSMutableURLRequest *urlRequest = [[super requestBySerializingRequest:request withParameters:parameters error:error] mutableCopy];
        [urlRequest setHTTPBody:[self.httpBody dataUsingEncoding:NSUTF8StringEncoding]];
        self.httpBody = nil;
        return urlRequest;
    }
    return [super requestBySerializingRequest:request withParameters:parameters error:error];
}

@end
