//
//  LocationsManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "LocationsManager.h"
#import "Location.h"

@implementation LocationsManager

+ (instancetype)sharedInstance
{
    static LocationsManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[LocationsManager alloc] init];
    });
    
    return sharedMyManager;
}

- (id)init{
    self = [super init];
    
    if (self) {
        self.locations = [[NSMutableArray alloc] init];
        self.groupedLocations = [[NSMutableDictionary alloc] init];
        self.selectedLocations = [[NSMutableArray alloc] init];
    }
    
    return self;
}

- (NSMutableArray*)getLocationsForFloor: (BuildingFloor)floor
{
    NSMutableArray *floorLocations = [NSMutableArray new];
    
    [self.locations enumerateObjectsUsingBlock:^(Location* obj, NSUInteger idx, BOOL *stop) {
        if (obj.floor == floor) {
            [floorLocations addObject:obj];
        }
    }];
    
    return floorLocations;
}

- (void)getGroupedLocations
{    
    NSMutableArray *keys = [NSMutableArray arrayWithArray:@[@(GroundFloor),@(FirstFloor),@(SecondFloor),@(ThirdFloor),@(FourthFloor)]];
    
    NSMutableDictionary *groupedLocations = [[NSMutableDictionary alloc] init];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"floor" ascending:YES];
    [self.locations sortUsingDescriptors:@[sortDescriptor]];
    
    for (NSString *key in keys) {
        [groupedLocations setObject:[[NSMutableArray alloc] init] forKey:key];
    }
    
    [keys enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [groupedLocations setObject:[self getLocationsForFloor:idx] forKey:[NSNumber numberWithInteger:idx]];
    }];
    
    self.groupedLocations = groupedLocations;
}


@end
