//
//  UserManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

/**
 The UserManager class and its extensions are meant to imitate the Friend class behavior
 in order to prevent classes that only import UserManager.h, access to reset user credentials.
 */
@interface UserManager : NSObject {
    @private
    User* cachedUser;
}

+ (instancetype)sharedInstance;

/**
 This method returns YES if user credentials are available
 */
- (BOOL)userHasActiveCredentials;

/**
 This method gets current active user's credentials
 */
- (User*)getActiveUser;

@end
