//
//  BookingsManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSBookingManager.h"
#import "EmailServiceManager.h"
#import "UserManager.h"

@protocol BookingsDelegate <NSObject>

@required
- (void)uploadFinished:(Booking *)booking;
- (void)wsRequestError:(NSError *)error;

@optional
- (void)bookingsRetrieved;

@end

@protocol SignInDelegate <NSObject>

@required
- (void)signInSuccessful;
- (void)wsRequestError:(NSError *)error;

@end

@interface BookingsManager : NSObject <DataRetrieverDelegate, EmailResponseDelegate>

+ (instancetype)sharedInstance;

/**
 This method sorts the bookings by date
 */
- (void)getBookingsGroupedByDate;

/**
 This method sorts the bookings by location
 */
- (void)getBookingsGroupedByLocation;

/**
 This method uploads the given booking
 @param booking - The booking to upload
 */
- (void)uploadBooking:(Booking *)booking;

/**
 This method uploads the given booking
 @param booking - The booking to upload
 */
- (void)editBooking:(Booking *)booking;

/**
 This method cancel the given booking
 @param booking - The booking to cancel
 */
- (void)cancelBooking:(Booking *)booking;

/**
 This method gets the bookings for all rooms
 */
- (void)getWSBookings;

- (void)checkUserItems;

@property (nonatomic, strong) NSMutableArray *bookings;
@property (nonatomic, strong) OrderedDictionary *myBookings;

@property (nonatomic, strong) OrderedDictionary *bookingsByDates;
@property (nonatomic, strong) OrderedDictionary *bookingsByLocation;

@property (nonatomic, strong) OrderedDictionary *currentBookings;
@property (nonatomic, strong) NSMutableArray *wfaBookings;

@property (nonatomic, weak) id<BookingsDelegate> bookingsDelegate;
@property (nonatomic, weak) id<SignInDelegate> signInDelegate;

@property (nonatomic, readonly) BOOL listNeedsUpdate;
@property (nonatomic, strong) Booking *currentEditedBooking;

@end
