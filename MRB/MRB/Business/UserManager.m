//
//  UserManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "UserManager.h"

@interface UserManager()

@property (nonatomic, strong) User* cachedUser;

@end

@implementation UserManager

@synthesize cachedUser = _cachedUser;

+ (instancetype)sharedInstance
{
    static UserManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[UserManager alloc] init];
    });
    
    return sharedManager;
}

- (id)init {
    
    if (self = [super init]) {
        NSData *userData = [[NSUserDefaults standardUserDefaults] objectForKey:@"user"];
        if (userData != nil) {
            _cachedUser = [NSKeyedUnarchiver unarchiveObjectWithData:userData];
        }
    }
    
    return self;
}

- (BOOL)userHasActiveCredentials
{
    return (_cachedUser != nil && ![_cachedUser.userName isEqualToString:@""]);
}

- (User *)getActiveUser
{
    return _cachedUser;
}

@end
