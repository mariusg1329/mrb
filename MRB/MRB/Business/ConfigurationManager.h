//
//  ConfigurationManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/21/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LDAPUser.h"

@interface ConfigurationManager : NSObject

@property (nonatomic, strong) LDAPUser* currentLDAP;

+ (instancetype)sharedInstance;
- (BOOL)clearUserCredentials;
- (void)saveConfiguration;
- (void)loadConfiguration;

@end
