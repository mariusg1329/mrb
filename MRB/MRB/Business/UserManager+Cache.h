//
//  UserManager+Cache.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "UserManager.h"

@interface UserManager (Cache)

@property (nonatomic, strong) User* cachedUser;

/**
 This method overwrites user credentials
 @param user - The new user credentials
 */
- (void)saveUserCredentials:(User*)user;

/**
 This method removes the user cache
 */
- (void)deleteUserCredentials;

@end
