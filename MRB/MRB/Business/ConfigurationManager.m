//
//  ConfigurationManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/21/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "ConfigurationManager.h"
#import "UserManager+Cache.h"

@interface ConfigurationManager ()

@end

@implementation ConfigurationManager

+ (instancetype)sharedInstance
{
    static ConfigurationManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[ConfigurationManager alloc] init];
    });
    
    return sharedManager;
}

- (id)init
{
    if (self = [super init]) {
        [self loadConfiguration];
    }
    
    return self;
}

- (void)saveConfiguration
{
    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:_currentLDAP];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:[[UserManager sharedInstance] getActiveUser].userName];
    }
}

- (void)loadConfiguration
{
    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[[UserManager sharedInstance] getActiveUser].userName];
        if (data) {
            _currentLDAP = [NSKeyedUnarchiver unarchiveObjectWithData:data];

        } else if (!_currentLDAP) {
            _currentLDAP = [LDAPUser new];
        }
    }
}

- (BOOL)clearUserCredentials
{
    if ([[UserManager sharedInstance] userHasActiveCredentials]) {
        [self saveConfiguration];
        [[UserManager sharedInstance] deleteUserCredentials];
        return YES;
    }
    
    return NO;
}

@end
