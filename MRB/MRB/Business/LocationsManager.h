//
//  LocationsManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GeneralUtils.h"

typedef NS_ENUM(NSUInteger, BuildingFloor) {
    GroundFloor,
    FirstFloor,
    SecondFloor,
    ThirdFloor,
    FourthFloor
};

@interface LocationsManager : NSObject

@property (nonatomic, strong) NSMutableArray *locations;
@property (nonatomic, strong) NSMutableDictionary *groupedLocations;
@property (nonatomic, strong) NSMutableArray *selectedLocations;


+ (instancetype)sharedInstance;

/**
 This method gets all the rooms for the given floor
 @param floor - The given floor
 */
- (NSMutableArray*)getLocationsForFloor: (BuildingFloor)floor;

/**
 This method gets all the rooms ordered alphabetically
 */
- (void)getGroupedLocations;

@end
