//
//  BookingsManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "BookingsManager.h"
#import "LocationsManager.h"
#import "Booking.h"
#import "FilterManager.h"
#import "DataParser.h"
#import "OrderedDictionary+Bookings.h"
#import "NSMutableArray+Bookings.h"

@interface BookingsManager ()

@property (nonatomic, strong) WSBookingManager *wsManager;

@end

@implementation BookingsManager

+ (instancetype)sharedInstance
{
    static BookingsManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedManager = [[BookingsManager alloc] init];
    });
    
    return sharedManager;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _bookings = [NSMutableArray new];
        _bookingsByDates = [OrderedDictionary new];
        _bookingsByLocation = [OrderedDictionary new];
        _wsManager = [WSBookingManager new];
        _wfaBookings = [NSMutableArray new];

        _wsManager.dataRetrieverDelegate = self;
        _listNeedsUpdate = NO;
    }
    
    return self;
}

- (void)getBookingsGroupedByDate
{
    OrderedDictionary *dict = [[OrderedDictionary alloc] init];
        
    for (Booking *booking in self.bookings) {
        NSDate *date = booking.startDate;
        
        if (date == nil) {
            date = [NSDate date];
        }
        
        NSDate *shortDate = [GeneralUtils trimmedDateForDate:date];
        
        if ([dict objectForKey:shortDate] == nil) {
            [dict insertObject:[NSMutableArray new] forDate:shortDate];
        }
        
        [[dict objectForKey:shortDate] addObject:booking];
    }
    
    self.bookingsByDates = dict;
    self.currentBookings = self.bookingsByDates;
}

- (void)getBookingsGroupedByLocation
{
    OrderedDictionary *dict = [[OrderedDictionary alloc] init];
    
    for (Booking *booking in self.bookings) {
        NSString *locationName = booking.location.name;
        
        if (locationName == nil) {
            locationName = @"";
        }
        
        if ([dict objectForKey:locationName] == nil) {            
            [dict insertObject:[NSMutableArray new] forLocation:locationName];
        }

        [[dict objectForKey:locationName] addObject:booking];
    }
    
    self.bookingsByLocation = dict;
    self.currentBookings = self.bookingsByLocation;
}

- (void)getMyBookings:(BOOL)withMapping
{
    if (withMapping) {
        
        BOOL found = NO;
        
        NSMutableArray *bookingsCopy = [[NSMutableArray alloc] initWithArray:self.bookings];
        NSMutableArray *wfaCopy = [[NSMutableArray alloc] initWithArray:self.wfaBookings];
        
        for (Booking *wfaBooking in wfaCopy) {
            for (Booking *booking in bookingsCopy) {
                
                if ([wfaBooking isEqualByID:booking]) {
                    
                    if (wfaBooking.isBeingCanceled) {
                        booking.isBeingCanceled = YES;
                        booking.isApproved = NO;
                    } else {
                        [self.wfaBookings removeObject:wfaBooking];
                        wfaBooking.isBeingEdited = NO;
                        wfaBooking.isBeingCanceled = NO;
                        wfaBooking.isApproved = YES;
                    }
                    
                    found = YES;
                }
                
            }
            
            if (found == NO) {
                if (wfaBooking.isBeingCanceled) {
                    [self.wfaBookings removeObject:wfaBooking];
                } else {
                    [self.bookings addBooking:wfaBooking];
                }
            }
        }
    }
    
    //Load the new bookings
    self.myBookings = [[FilterManager instance] filterBookingsForOrganizer:[[UserManager sharedInstance] getActiveUser].userName];
}

#pragma mark - WS Manager methods

- (void)uploadBooking:(Booking *)booking
{
    [self.wsManager uploadBookingWithRequest:booking request:REQUEST_TYPE_REQUEST status:STATUS_CONFIRMED isEditing:NO];
}

- (void)editBooking:(Booking *)booking
{
    [self.wsManager uploadBookingWithRequest:booking request:REQUEST_TYPE_CANCEL status:STATUS_CANCELED isEditing:YES];
}

- (void)cancelBooking:(Booking *)booking
{
    [self.wsManager uploadBookingWithRequest:booking request:REQUEST_TYPE_CANCEL status:STATUS_CANCELED isEditing:NO];
}

- (void)getWSBookings
{
    [self.wsManager cancelService];
    [self.wsManager performSelector:@selector(downloadAllBookingsUsingJSON) withObject:nil afterDelay:1];
}

- (void)checkUserItems
{
    [self.wsManager downloadMyBookings];
}

- (void)downloadDidFinish:(NSMutableArray *)dataArray
{
    self.bookings = dataArray;
    
    [self getMyBookings:YES];
    _listNeedsUpdate = NO;
    
    if ([self.signInDelegate respondsToSelector:@selector(signInSuccessful)]) {
        [self.signInDelegate signInSuccessful];
        return;
    }
    
    if ([self.bookingsDelegate respondsToSelector:@selector(bookingsRetrieved)]) {
        [self.bookingsDelegate bookingsRetrieved];
    }
}

- (void)signInFailedWithError:(NSError *)error
{
    if ([self.signInDelegate respondsToSelector:@selector(wsRequestError:)]) {
        [self.signInDelegate wsRequestError:error];
    }
}

- (void)requestFailedWithError:(NSError *)error
{
    if ([self.bookingsDelegate respondsToSelector:@selector(wsRequestError:)]) {
        [self.bookingsDelegate wsRequestError:error];
    }
}

#pragma mark - Email service

- (void)emailSent:(Booking *)booking requestMethod:(RequestMethod)request
{    
    booking.isApproved = NO;

    if (request == RequestMethodAdd) {
        [self.bookings addBooking:booking];
    } else if (request == RequestMethodCancel) {
        booking.isBeingCanceled = YES;
    }
    
    [self.wfaBookings addObject:booking];
    
    [self getMyBookings:NO];
    _listNeedsUpdate = YES;
    
    if ([self.bookingsDelegate respondsToSelector:@selector(uploadFinished:)]) {
        [self.bookingsDelegate uploadFinished:booking];
     }
}

- (void)emailFailedWithError:(NSError *)error
{
    if ([self.bookingsDelegate respondsToSelector:@selector(wsRequestError:)]) {
        [self.bookingsDelegate wsRequestError:error];
    }
}

@end
