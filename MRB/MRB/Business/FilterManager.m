//
//  FilterManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 8/30/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "FilterManager.h"
#import "BookingsManager.h"
#import "Booking.h"
#import "UserManager.h"
#import "GeneralUtils.h"
#import "OrderedDictionary+Bookings.h"
#import "NSMutableArray+Bookings.h"

@implementation FilterManager

+ (instancetype)instance
{
    static FilterManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[FilterManager alloc] init];
    });
    
    return sharedMyManager;
}

- (id)init
{
    if (self = [super init]) {
        _dateFilterArray = [NSMutableArray new];
        _roomFilterArray = [NSMutableArray new];
    }
    
    return self;
}

- (void)applyFilters
{
    if (self.roomFilterArray.count > 0) {
        [BookingsManager sharedInstance].currentBookings = [self filterBookingsUsingLocations:[BookingsManager sharedInstance].currentBookings];
    }
    
    if (self.dateFilterArray.count > 0) {
        [BookingsManager sharedInstance].currentBookings = [self filterBookingsUsingDates:[BookingsManager sharedInstance].currentBookings];
    }
}

- (OrderedDictionary *)filterBookingsUsingDates:(OrderedDictionary*)bookings
{
    if (self.dateFilterArray.count == 0) {
        return bookings;
    }
    
    NSDate *fromDate = [self.dateFilterArray objectAtIndex:0];
    NSDate *toDate = [GeneralUtils extendedDateForDate:[self.dateFilterArray objectAtIndex:1]];
    
    
    if (self.dateFilterArray.count == 0 || [fromDate compare:toDate] == NSOrderedDescending) {
        return bookings;
    }
    
    OrderedDictionary *dataSource = [OrderedDictionary new];

    [bookings enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSArray *bookingsArray, BOOL *stop) {
       
        
        for (Booking *booking in bookingsArray) {
            if ([booking.startDate compare:fromDate] != NSOrderedAscending
                && [booking.endDate compare:toDate] != NSOrderedDescending) {
                
                
                if ([dataSource objectForKey:key] == nil) {
                    [dataSource setObject:[NSMutableArray new] forKey:key];
                }
                
                NSMutableArray *arr = [dataSource objectForKey:key];
                [arr addObject:booking];
            }
        }
        
    }];

    return dataSource;
}

- (OrderedDictionary *)filterBookingsUsingLocations:(OrderedDictionary*)bookings
{
    if (self.roomFilterArray.count == 0) {
        return bookings;
    }
    
    __block OrderedDictionary *dataSource = [OrderedDictionary new];
    
    [bookings enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSArray *bookingsArray, BOOL *stop) {
        
        for (Booking *booking in bookingsArray) {
            if ([self.roomFilterArray containsObject:booking.location.name]) {
                
                if ([dataSource objectForKey:key] == nil) {
                    [dataSource setObject:[NSMutableArray new] forKey:key];
                }
                
                NSMutableArray *arr = [dataSource objectForKey:key];
                [arr addObject:booking];
            }
        }
        
    }];
    
    return dataSource;
}

- (OrderedDictionary *)filterBookingsForOrganizer:(NSString *)organizer
{
    __block OrderedDictionary *dataSource = [OrderedDictionary new];
    
    for (Booking *booking in [BookingsManager sharedInstance].bookings) {
        
        if ([booking.organizer isEqualToString:organizer]) {
            
            if (booking.isApproved == NO) {
                
                if ([dataSource objectForKey:WFA_APPOINTMENTS_HEADER] == nil) {
                    [dataSource insertObject:[NSMutableArray new] forKey:WFA_APPOINTMENTS_HEADER atIndex:0];
                }
                
                [[dataSource objectForKey:WFA_APPOINTMENTS_HEADER] addObject:booking];
                
                if (![[BookingsManager sharedInstance].wfaBookings containsBooking:booking]) {
                    [[BookingsManager sharedInstance].wfaBookings addObject:booking];
                }
                
            } else {
                NSDate *shortDate = [GeneralUtils trimmedDateForDate:booking.startDate];
                
                if ([dataSource objectForKey:shortDate] == nil) {
                    [dataSource insertObject:[NSMutableArray new] forDate:shortDate];
                }
                
                [[dataSource objectForKey:shortDate] addObject:booking];
            }
            
        }
    }
    
    return dataSource;
}


@end
