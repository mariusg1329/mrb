//
//  UserManager+Cache.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "UserManager+Cache.h"

@interface UserManager()

@property (nonatomic, strong) User *cachedUser;

@end

@implementation UserManager (Cache)

- (void)deleteUserCredentials
{
    self.cachedUser = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"user"];
}

- (void)saveUserCredentials:(User*)user
{
    self.cachedUser = user;
    
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[UserManager sharedInstance].cachedUser];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
}

@end
