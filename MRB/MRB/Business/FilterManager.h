//
//  FilterManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 8/30/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FilterManager : NSObject

/**
 This method is used to sort the bookings by organizer string
 @param organizer - The given organizer string
 */
- (OrderedDictionary *)filterBookingsForOrganizer:(NSString *)organizer;

/**
 This method applies all exisisting filters (see dateFilterArray and roomFilterArray)
 */
- (void)applyFilters;

@property (nonatomic, strong) NSMutableArray *dateFilterArray;
@property (nonatomic, strong) NSMutableArray *roomFilterArray;

+ (instancetype)instance;

@end
