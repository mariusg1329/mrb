//
//  EmailSearchView.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/16/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "EmailSearchView.h"

@interface EmailSearchView ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeight;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarY;

@end

@implementation EmailSearchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setup];
    }
    return self;
}

- (void)awakeFromNib
{
    [self setup];
}

- (void)setup
{
    _dataSource = [NSMutableArray new];
    self.resultsTable.dataSource = self;
    self.resultsTable.delegate = self;
    self.resultsTable.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.resultsTable.tableHeaderView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [self setBackgroundColor:[[UIColor lightGrayColor] colorWithAlphaComponent:0.65]];
    
    //done button
    for (UIView *subview in self.searchBar.subviews) {
        for (UIView *subSubview in subview.subviews) {
            if ([subSubview conformsToProtocol:@protocol(UITextInputTraits)]) {
                UIView<UITextInputTraits> *textInputField = (UIView<UITextInputTraits> *)subSubview;
                textInputField.returnKeyType = UIReturnKeyDone;
                break;
            }
        }
    }
    
    if ([[UIDevice currentDevice].systemVersion floatValue] < IOS_7) {
        self.searchBarY.constant = 0;
    }
    
    self.cancelButton.backgroundColor = self.backgroundColor;
    self.shouldEndEditting = NO;
    [self.searchBar becomeFirstResponder];
    
    self.emptyLabel.backgroundColor = self.backgroundColor;
    self.emptyLabel.hidden = NO;
    self.emptyLabel.text = EMAIL_SEARCH_CHARACTERS;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString* cellIdentifier = @"suggestionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    UILabel *suggestionLabel = cell.textLabel;
    suggestionLabel.textAlignment = NSTextAlignmentCenter;
    
    NSString *email = [[[self.dataSource objectAtIndex:indexPath.row] valueForKey:@"mail"] objectAtIndex:0];
    suggestionLabel.text = email;
    [suggestionLabel sizeToFit];
    
    CGRect frame = suggestionLabel.frame;
    frame.origin.x = cell.frame.size.width - suggestionLabel.frame.size.width / 2;
    frame.size.height = cell.frame.size.height - 1;
    suggestionLabel.frame = frame;
    suggestionLabel.backgroundColor = [UIColor clearColor];
    
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    self.tableHeight.constant = 0;
    
    if (self.dataSource.count > MAX_DISPLAYED_RESULTS) {
        self.tableHeight.constant += MAX_DISPLAYED_RESULTS * ATTENDEES_CELL_HEIGHT + ATTENDEES_CELL_HEIGHT / 2;
    } else if(self.dataSource.count == 0) {
        self.tableHeight.constant = 0;
    } else {
        self.tableHeight.constant += self.dataSource.count * ATTENDEES_CELL_HEIGHT;
    }

    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
    }];
    
    return self.dataSource.count;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.dataSource containsObject:self.searchBar.text]) {
        self.searchBar.text = [[[self.dataSource objectAtIndex:indexPath.row] valueForKey:@"mail"] objectAtIndex:0];
    }
}

- (IBAction)cancelPressed:(id)sender
{
    self.shouldEndEditting = YES;
    [self removeFromSuperview];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ATTENDEES_CELL_HEIGHT;
}

@end
