//
//  CustomDatePicker.h
//  MRB
//
//  Created by Ghita Marius Lucian on 8/25/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    DateType,
    TimeType,
    DateAndTimeType,
} Type;

@protocol CustomDatePickerDelegate <NSObject>

- (void)dateChanged:(id)sender withRow:(NSInteger)row inComponent:(NSInteger)component;

@end


@interface CustomDatePicker : UIControl {
    @private
    NSDate *date;
}

@property (nonatomic, strong) id<CustomDatePickerDelegate> pickerDelegate;
@property (nonatomic) Type pickerType;


@property (nonatomic, strong) NSDate *minDate;
@property (nonatomic, strong) NSDate *maxDate;

@property (nonatomic, strong) NSString *dateFormat;

- (void)scrollAtRowInComponent:(NSInteger)row component:(NSInteger)component;
- (void)scrollToRowWithDate:(NSDate*)date;

- (NSInteger)getPickerSelectedRow;

- (void)reloadData;

@end
