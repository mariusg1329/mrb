//
//  CustomDatePicker.m
//  MRB
//
//  Created by Ghita Marius Lucian on 8/25/14.
//  Copyright (c) 2014 work. All rights reserved.
//


#import "CustomDatePicker.h"

@interface CustomDatePicker() <UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *picker;
@property (nonatomic, strong) NSCalendar *calendar;
@property (nonatomic, strong) NSDate *date;

@end

#define NUMBER_OF_DAYS 365
#define DEFAULT_DATE_FORMAT "d MMM YYYY"

@implementation CustomDatePicker

@synthesize date = _date;

- (void)reloadData
{
    [self setup];
}
- (void)setup
{
    if (self.picker) {
        [self.picker removeFromSuperview];
    } else {
        self.backgroundColor = [UIColor whiteColor];
        
        self.calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        
        self.picker = [[UIPickerView alloc] initWithFrame:self.bounds];
        self.picker.backgroundColor = [UIColor clearColor];
        self.picker.dataSource = self;
        self.picker.delegate = self;
    }
    
    if (_date == nil) {
        _date = [NSDate date];
    }
    
    if (self.minDate == nil) {
        self.minDate = [NSDate date];
    }
    
    if (_pickerType != DateType) {
        NSDateComponents *components = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:_date];

        NSInteger hour = [components hour];
        NSInteger minute = [components minute];
        
        [self.picker selectRow:hour inComponent:1 animated:NO];
        [self.picker selectRow:minute inComponent:2 animated:NO];
        [self.picker selectRow:0 inComponent:0 animated:NO];
    } else {
        [self scrollToRowWithDate:_date];
    }
    
    [self addSubview:self.picker];
    
}

- (NSInteger)getPickerSelectedRow
{
    return [self.picker selectedRowInComponent:0];
}

- (void)scrollToRowWithDate:(NSDate*)prevDate
{
    NSDateComponents *components = [self.calendar components:(NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:prevDate];
    
    NSDate *fromDate;
    NSDate *toDate;
    
    [self.calendar rangeOfUnit:NSDayCalendarUnit startDate:&fromDate interval:NULL forDate:self.minDate];
    [self.calendar rangeOfUnit:NSDayCalendarUnit startDate:&toDate interval:NULL forDate:prevDate];
        
    if (_pickerType != DateType) {
        [_picker selectRow:components.hour inComponent:1 animated:YES];
        [_picker selectRow:components.minute inComponent:2 animated:YES];
    }
    
    components = [self.calendar components:NSDayCalendarUnit fromDate:fromDate toDate:toDate options:0];
    [_picker selectRow:components.day inComponent:0 animated:YES];
}

- (void)scrollAtRowInComponent:(NSInteger)row component:(NSInteger)component
{
    [self.picker selectRow:row inComponent:component animated:YES];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (_pickerType == DateAndTimeType) {
        return 3;
    }
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    switch (component) {
        case 0:
            return NUMBER_OF_DAYS;
        case 1:
            return 24;
        case 2:
            return 4;
        default:
            return 1;
    }
    
}

-(CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return 150;
            break;
        case 1:
            return 60;
            break;
        case 2:
            return 60;
            break;
        default:
            return 0;
            break;
    }
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 35;
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lblDate = [[UILabel alloc] init];
    [lblDate setFont:[UIFont systemFontOfSize:15.0]];
    [lblDate setTextColor:[UIColor blueColor]];
    [lblDate setBackgroundColor:[UIColor clearColor]];
    lblDate.textAlignment = NSTextAlignmentCenter;

    if (component == 0) // Date
    {
        NSDate *rowDate = [NSDate dateWithTimeInterval:row*24*60*60 sinceDate:self.minDate];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.locale = [NSLocale currentLocale];
        
        if (!self.dateFormat) {
            formatter.dateFormat = @DEFAULT_DATE_FORMAT;
        } else {
            formatter.dateFormat = self.dateFormat;
        }
        
        [lblDate setText:[formatter stringFromDate:rowDate]];
    } else if (component == 1) { // Hour
        int max = (int)[self.calendar maximumRangeOfUnit:NSHourCalendarUnit].length;
        [lblDate setText:[NSString stringWithFormat:@"%02ld",(row % max)]];
    } else if (component == 2) { // Minutes
        
        switch (row) {
            case 0:
                [lblDate setText:@"00"];
                break;
            case 1:
                [lblDate setText:@"15"];
                break;
            case 2:
                [lblDate setText:@"30"];
                break;
            case 3:
                [lblDate setText:@"45"];
                break;
            default:
                break;
        }
    }

    return lblDate;
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger daysFromStart;
    NSDate *chosenDate;
    
    daysFromStart = [pickerView selectedRowInComponent:0];
    chosenDate = [NSDate dateWithTimeInterval:daysFromStart*24*60*60 sinceDate:self.minDate];
    
    NSDateComponents *components = [self.calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:chosenDate];

    if (_pickerType != DateType) {
        NSInteger hour = [pickerView selectedRowInComponent:1];
        NSInteger minute = [pickerView selectedRowInComponent:2];
        
        components.hour = hour;
        components.minute = minute;
    }
    
    // Build date out of the components we got
    _date = [self.calendar dateFromComponents:components];
    
    if ([self.pickerDelegate respondsToSelector:@selector(dateChanged:withRow:inComponent:)]) {
        [self.pickerDelegate dateChanged:self withRow:row inComponent:component];
    }
}

@end
