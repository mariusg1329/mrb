//
//  CustomDatePicker+Date.m
//  MRB
//
//  Created by Ghita Marius Lucian on 8/27/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "CustomDatePicker+Date.h"

@interface CustomDatePicker ()

@property (nonatomic, strong) NSDate* date;

@end

@implementation CustomDatePicker (Date)


@end
