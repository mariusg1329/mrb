//
//  LoadingView.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/7/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "LoadingView.h"

@interface LoadingView()

@end

@implementation LoadingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        CGRect frame = self.frame;
        frame.origin.x = 0;
        frame.origin.y = 0;
        self.frame = frame;
        
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.backgroundColor = [UIColor grayColor];
    self.opaque = NO;
    self.alpha = 0.5;
    self.userInteractionEnabled = NO;

    UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.center = self.center;
    spinner.hidden = NO;
    [spinner startAnimating];
    
    [self addSubview:spinner];
}

@end
