//
//  EmailSearchView.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/16/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmailSearchView : UIView <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *dataSource;

@property (nonatomic, weak) IBOutlet UISearchBar *searchBar;
@property (nonatomic, weak) IBOutlet UITableView *resultsTable;
@property (nonatomic, weak) IBOutlet UILabel *emptyLabel;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic) BOOL shouldEndEditting;

@end
