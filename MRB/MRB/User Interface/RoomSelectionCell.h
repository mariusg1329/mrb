//
//  RoomSelectionCellTableViewCell.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/1/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoomSelectionCell : UITableViewCell

@property (nonatomic, strong) UIButton *checkButton;

@end
