//
//  RoomSelectionCellTableViewCell.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/1/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "RoomSelectionCell.h"

@implementation RoomSelectionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    self.checkButton.selected = selected;
}

@end
