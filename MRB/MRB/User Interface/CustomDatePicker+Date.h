//
//  CustomDatePicker+Date.h
//  MRB
//
//  Created by Ghita Marius Lucian on 8/27/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "CustomDatePicker.h"

@interface CustomDatePicker (Date)

@property (nonatomic, strong) NSDate* date;

@end
