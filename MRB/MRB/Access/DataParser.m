//
//  DataParser.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/2/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "DataParser.h"
#import "Booking.h"
#import "Location.h"
#import "NSMutableArray+Bookings.h"

@implementation DataParser

+ (void)parseBookingsUsingJSON:(NSData *)data toArray:(NSMutableArray *)bookings
{
    NSDictionary *JSONDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    
     NSArray *firstDict = [JSONDict valueForKey:@"appt"];
    
    for (NSDictionary *dict in firstDict) {
        
        NSArray *booking = [[[[dict valueForKey:@"inv"] objectAtIndex:0] valueForKey:@"comp"] objectAtIndex:0];
        
        NSString *title = [booking valueForKey:@"name"];
        NSString *bookingID = [booking valueForKey:@"uid"];
        NSString *organizer = [booking valueForKeyPath:@"or.a"];
        NSString *location = [[booking valueForKey:@"loc"] stringByReplacingOccurrencesOfString:ZIMBRA_EMAIL_TAG withString:@""];
        location = [self matchingLocation:location];
        location = [GeneralUtils upperCaseFirstLetter:location];

        NSArray *att = [booking valueForKey:@"at"];
        NSMutableArray *attendees = [NSMutableArray new];
        
        for (NSDictionary *attendee in att) {
            [attendees addObject:[attendee valueForKeyPath:@"a"]];
        }
        
        NSString *startDate = [[[booking valueForKeyPath:@"s"] objectAtIndex:0] valueForKey:@"d"];
        NSString *endDate = [[[booking valueForKeyPath:@"e"] objectAtIndex:0] valueForKey:@"d"];

        NSString *description = [[[booking valueForKey:@"desc"] objectAtIndex:0] valueForKey:@"_content"];
        Booking *b = [[Booking alloc] initWithData:startDate
                                           endDate:endDate
                                          uniqueID:bookingID
                                           summary:title
                                       description:description
                                          location:location
                                         organizer:organizer
                                         attendees:attendees];
        
        [bookings addBooking:b];
    }
}

+ (NSString *)matchingLocation:(NSString *)locationName
{
    locationName = [locationName stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    for (Location *loc in [LocationsManager sharedInstance].locations) {
        
        NSString *location = [loc.name lowercaseString];
        location = [location stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        if ([locationName rangeOfString:location options:NSCaseInsensitiveSearch].location != NSNotFound) {
            return location;
        }
    }
    
    return locationName;
}

+ (NSMutableArray*)parseAttendeeString:(NSString *)attendeeString
{
    NSMutableArray *attendees = [[NSMutableArray alloc] init];
    attendees = (NSMutableArray*)[attendeeString componentsSeparatedByString:@"mailto"];
    
    if (attendees.count > 0) [attendees removeObjectAtIndex:0];
    else return attendees;
    NSMutableArray *finalArray = [NSMutableArray new];
    
    for (int i=0 ; i<attendees.count; i++) {
        NSString *trimmed = [attendees objectAtIndex:i];
        NSScanner *scanner = [[NSScanner alloc] initWithString:trimmed];
        [scanner scanUpToString:@":" intoString:nil];
        [scanner scanUpToString:@"\r" intoString:&trimmed];
        
        trimmed = [trimmed stringByReplacingOccurrencesOfString:@":" withString:@""];
        
        [finalArray addObject:trimmed];
    }

    return finalArray;
}

+ (NSString*)bookingToICS:(Booking*)booking method:(NSString*)method status:(NSString*)status
{
    NSString *icsString = @"";
    icsString = @"BEGIN:VCALENDAR\nPRODID:-//Microsoft Corporation//Outlook 9.0 MIMEDIR//EN\nVERSION:2.0\n";
    
    icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"METHOD:%@\nBEGIN:VEVENT\nUID:%@\nSUMMARY:%@\nDESCRIPTION:%@\nLOCATION:%@\n",method, booking.bookingId, booking.title, booking.description, [[booking.location.name lowercaseString] stringByAppendingString:ZIMBRA_EMAIL_TAG]]];
    
    //Add attendees
    for (NSString *attendee in booking.attendees) {
        icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"ATTENDEE;CN=%@;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE:\nmailto:%@\n", attendee, attendee]];
    }
    
    //Add room
    NSString *roomFormat = [[booking.location.name stringByReplacingOccurrencesOfString:@" " withString:@""] lowercaseString];
    
    icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"ATTENDEE;CN=\"Floor: %lu\";CUTYPE=RESOURCE;ROLE=NON-PARTICIPANT;PARTSTAT=ACCEPTED;RSVP=TRUE:\nmailto:%@%@\n",booking.location.floor, roomFormat,ZIMBRA_EMAIL_TAG]];
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.dateFormat = @"yyyyMMdd'T'HHmmss";
    
    icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"ORGANIZER;CN=%@:mailto:%@\nDTSTART;TZID=\"Asia/Beirut\":%@\nDTEND;TZID=\"Asia/Beirut\":%@\n",booking.organizer, booking.organizer, [dateFormat stringFromDate:booking.startDate], [dateFormat stringFromDate:booking.endDate]]];
    
    icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"STATUS:%@\n",status]];
    
    icsString = [icsString stringByAppendingString:[NSString stringWithFormat:@"CLASS:PUBLIC\nTRANSP:OPAQUE\nLAST-MODIFIED:%@\nDTSAMP:%@\nSEQUENCE:0\nBEGIN:VALARM\nACTION:DISPLAY\nTRIGGER;RELATED=START:PT5M\nEND:VALARM\nEND:VEVENT\nEND:VCALENDAR", [dateFormat stringFromDate:[NSDate date]], [dateFormat stringFromDate:[NSDate date]]]];
    
    return icsString;
}

#pragma mark - Local parsing

+ (NSInteger)parseLocalizedErrorMSG:(NSError *)errorMSG
{
    NSScanner *errorScanner;
    
    NSString *output;
    
    if (errorMSG.domain == NSURLErrorDomain) {
        errorScanner = [NSScanner scannerWithString:errorMSG.description];
        [errorScanner scanUpToString:@"Code=" intoString:nil];
        [errorScanner scanUpToString:@" " intoString:&output];
        output = [output stringByReplacingOccurrencesOfString:@"Code=" withString:@""];
    } else {
        NSDictionary *userInfo = errorMSG.userInfo;
        NSString *errorString = [userInfo objectForKey:@"NSLocalizedDescription"];
        errorScanner = [NSScanner scannerWithString:errorString];
        [errorScanner scanUpToString:@"(" intoString:nil];
        [errorScanner scanUpToString:@")" intoString:&output];
        output = [output stringByReplacingOccurrencesOfString:@"(" withString:@""];
    }
    
    return [output integerValue];
}

@end
