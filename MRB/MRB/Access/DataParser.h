//
//  DataParser.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/2/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Booking;
@interface DataParser : NSObject

/**
 Parse WS response object to get a better structured data
 */
+ (void)parseBookings:(NSData *)data toArray:(NSMutableArray*)bookings;

/**
 Create ICS string from a booking
 */
+ (NSString*)bookingToICS:(Booking*)booking method:(NSString*)method status:(NSString*)status;

/**
 Parse error object to get a better understanding of it
 */
+ (NSInteger)parseLocalizedErrorMSG:(NSError *)errorMSG;


/**
 Parse JSON response object to get a better structured data
 */
+ (void)parseBookingsUsingJSON:(NSData *)data toArray:(NSMutableArray *)bookings;

@end
