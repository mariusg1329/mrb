//
//  EmailServiceManager.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/16/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "EmailServiceManager.h"
#import "UserManager.h"
#import "DataParser.h"
#import "Booking.h"
#import "BookingsManager.h"

@implementation EmailServiceManager

+ (instancetype)sharedInstance
{
    static EmailServiceManager *emailManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        emailManager = [[EmailServiceManager alloc] init];
    });
    
    return emailManager;
}

- (id)init
{
    if (self = [super init]) {
        self.responseDelegate = [BookingsManager sharedInstance];
    }
    
    return self;
}

- (void)sendEmailWithRequest:(Booking *)booking attachment:(NSString *)attachment method:(NSString *)method status:(NSString *)status isEditing:(BOOL)isEditing
{
    User *currentUser = [[UserManager sharedInstance] getActiveUser];
    
    SKPSMTPBookings *emailMessage = [[SKPSMTPBookings alloc] init];
    emailMessage.fromEmail = currentUser.userName;
    emailMessage.wantsSecure = YES;
    emailMessage.validateSSLChain = NO;
    emailMessage.relayHost = ZIMBRA_SMTP_SERVER;
    emailMessage.requiresAuth = YES;
    emailMessage.connectTimeout = 5;
    emailMessage.login = currentUser.userName;
    emailMessage.pass = currentUser.password;
    emailMessage.subject = booking.title;
    emailMessage.uploadedBooking = booking;

    if ([method isEqual: REQUEST_TYPE_CANCEL]) {
        emailMessage.method = RequestMethodCancel;
        emailMessage.subject = [NSString stringWithFormat:@"Cancelled: %@",booking.title];
    } else {
        emailMessage.method = RequestMethodAdd;
    }
    
    if (isEditing) {
        emailMessage.method = RequestMethodEdit;
    }
    
    emailMessage.delegate = self;
    
    NSDictionary *icsInvitation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"text/calendar; charset=utf-8; method=%@; name=meeting.ics", method], kSKPSMTPPartContentTypeKey, @"7bit", kSKPSMTPPartContentTransferEncodingKey, attachment, kSKPSMTPPartMessageKey, nil];
    emailMessage.parts = @[icsInvitation];
    
    NSString *roomEmail = [NSString stringWithFormat:@"%@%@", [booking.location.name lowercaseString], ZIMBRA_EMAIL_TAG];
    roomEmail = [roomEmail stringByReplacingOccurrencesOfString:@" " withString:@""];
    emailMessage.toEmail = roomEmail;
    [emailMessage send];
    
    //Unfortunately sending multiple emails with the same SKPSMTP instance won't work :(
    for (NSString *attendee in booking.attendees) {
        
        if ([attendee isEqualToString:roomEmail]) {
            continue;
        }
        
        SKPSMTPBookings *emailMessage = [[SKPSMTPBookings alloc] init];
        emailMessage.fromEmail = currentUser.userName;
        emailMessage.wantsSecure = YES;
        emailMessage.validateSSLChain = NO;
        emailMessage.relayHost = ZIMBRA_SMTP_SERVER;
        emailMessage.requiresAuth = YES;
        emailMessage.connectTimeout = 5;
        emailMessage.login = currentUser.userName;
        emailMessage.pass = currentUser.password;
        emailMessage.subject = booking.title;
        emailMessage.uploadedBooking = booking;
        
        if ([method isEqual: REQUEST_TYPE_CANCEL]) {
            emailMessage.method = RequestMethodCancel;
            emailMessage.subject = [NSString stringWithFormat:@"Cancelled: %@",booking.title];
        } else if (booking.isBeingEdited){
            emailMessage.method = RequestMethodEdit;
        } else {
            emailMessage.method = RequestMethodAdd;
        }
        
        NSDictionary *icsInvitation = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"text/calendar; charset=utf-8; method=%@; name=meeting.ics", method], kSKPSMTPPartContentTypeKey, @"7bit", kSKPSMTPPartContentTransferEncodingKey, attachment, kSKPSMTPPartMessageKey, nil];
        emailMessage.parts = @[icsInvitation];
        
        emailMessage.toEmail = attendee;
        [emailMessage send];
    }

}

- (void)messageSent:(SKPSMTPMessage *)message
{
    SKPSMTPBookings *bMessage = (SKPSMTPBookings *)message;
    
    if (bMessage.uploadedBooking.isBeingEdited == YES && bMessage.uploadedBooking.isBeingCanceled == YES) {
        [[BookingsManager sharedInstance].bookings removeObject:bMessage.uploadedBooking];
        [[BookingsManager sharedInstance].wfaBookings removeObject:bMessage.uploadedBooking];
        bMessage.uploadedBooking.isBeingCanceled = NO;
        [[BookingsManager sharedInstance].currentEditedBooking setBookingId:[[NSUUID UUID] UUIDString]];
        [[BookingsManager sharedInstance] uploadBooking:[BookingsManager sharedInstance].currentEditedBooking];
        return;
    }
    
    if ([self.responseDelegate respondsToSelector:@selector(emailSent:requestMethod:)]) {
        [self.responseDelegate emailSent:bMessage.uploadedBooking requestMethod:bMessage.method];
    }
    NSLog(@"EMAIL SENT TO: %@", message.toEmail);
}

- (void)messageFailed:(SKPSMTPMessage *)message error:(NSError *)error
{
    if ([self.responseDelegate respondsToSelector:@selector(emailFailedWithError:)]) {
        [self.responseDelegate emailFailedWithError:error];
    }
}

@end
