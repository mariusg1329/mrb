//
//  DataRetriever.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/2/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MessageUI/MFMailComposeViewController.h>

#import "SKPSMTPMessage.h"
@class Booking;
@class AFHTTPRequestOperationManager;

@protocol DataRetrieverDelegate <NSObject>

/**
 Notify the receiver that the download has finished
 @param dataArray - Pass downloaded data
 */
- (void)downloadDidFinish:(NSArray *)dataArray;

/**
 Notify the receiver that the booking upload was a success
 @param booking - The booking that was successfuly uploaded
 */
//- (void)uploadFinishedForBooking:(Booking *)booking;

/**
 Notify the receiver that the request failed
 @param error - The error passed to the receiver
 */
- (void)requestFailedWithError:(NSError *)error;

- (void)signInFailedWithError:(NSError *)error;

@end

#define REQUEST_TYPE_CANCEL @"CANCEL"
#define REQUEST_TYPE_REQUEST @"REQUEST"
#define REQUEST_TYPE_ADD @"ADD"
#define REQUEST_TYPE_PUBLISH @"PUBLISH"

#define STATUS_CANCELED @"CANCELED"
#define STATUS_TENTATIVE @"TENTATIVE"
#define STATUS_CONFIRMED @"CONFIRMED"

@interface WSBookingManager : NSObject

@property (nonatomic,weak) id<DataRetrieverDelegate> dataRetrieverDelegate;
@property (nonatomic,strong) AFHTTPRequestOperationManager *manager;


/**
 Download my bookings
 */
- (void)downloadMyBookings;

/**
 Post ICalendar meeting
 */
//- (void)uploadBooking:(Booking*)booking;

/**
 Cancel all the enqueued request operations
 */
- (void)cancelService;

//JSON
- (void)downloadAllBookingsUsingJSON;

//Post ICalendar meeting
- (void)uploadBookingWithRequest:(Booking *)booking request:(NSString *)requestType status:(NSString *)status isEditing:(BOOL)isEditing;

@end
