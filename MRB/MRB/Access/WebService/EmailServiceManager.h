//
//  EmailServiceManager.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/16/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SKPSMTPBookings.h"

@class Booking;

@protocol EmailResponseDelegate <NSObject>

- (void)emailSent:(Booking *)booking requestMethod:(RequestMethod)request;
- (void)emailFailedWithError:(NSError *)error;

@end

@interface EmailServiceManager : NSObject <SKPSMTPMessageDelegate>

@property (nonatomic, weak) id<EmailResponseDelegate> responseDelegate;

+ (instancetype)sharedInstance;
- (void)sendEmailWithRequest:(Booking *)booking attachment:(NSString *)attachment method:(NSString *)method status:(NSString *)status isEditing:(BOOL)isEditing;

@end
