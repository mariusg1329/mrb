//
//  DataRetriever.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/2/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "WSBookingManager.h"
#import "Booking.h"
#import "AFNetworking.h"
#import "AFHTTPCustomRequestSerializer.h"
#import "DataParser.h"
#import "BookingsManager.h"
#import "UserManager.h"
#import "User.h"
#import "EmailServiceManager.h"

@interface WSBookingManager () {
    
    NSMutableArray *dataArray;
    NSString *calendarURL;
    dispatch_queue_t queue;
    Booking *uploadedBooking;
}
@end

@implementation WSBookingManager

- (id)init
{
    if (self = [super init]) {
        dataArray = [[NSMutableArray alloc] init];
        self.manager = [AFHTTPRequestOperationManager manager];
        [self setup];
    }
    
    return self;
}

#pragma mark - Private methods

- (void)setup
{
    [self.manager.securityPolicy setAllowInvalidCertificates:YES];
    
    AFHTTPCustomRequestSerializer *requestSerializer = [AFHTTPCustomRequestSerializer serializer];
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];

    responseSerializer.stringEncoding = NSUTF8StringEncoding;
    responseSerializer.acceptableContentTypes = [NSMutableSet setWithObjects:@"text/calendar", @"text/json", @"text/plain", @"text/javascript", @"application/json", nil];

    self.manager.responseSerializer = responseSerializer;
    self.manager.requestSerializer = requestSerializer;
}

#pragma mark - ICS operations

- (void)uploadBookingWithRequest:(Booking *)booking request:(NSString *)requestType status:(NSString *)status isEditing:(BOOL)isEditing
{
    NSString *icsString = [DataParser bookingToICS:booking method:requestType status:status];
    
    AFHTTPCustomRequestSerializer *requestSerializer = [AFHTTPCustomRequestSerializer serializer];
    AFHTTPResponseSerializer *responseSerializer = [AFHTTPResponseSerializer serializer];
    User *user = [[UserManager sharedInstance] getActiveUser];
    [requestSerializer setAuthorizationHeaderFieldWithUsername:user.userName password:user.password];
    [requestSerializer setHttpBody:icsString];
    responseSerializer.stringEncoding = NSUTF8StringEncoding;
    responseSerializer.acceptableContentTypes = [NSMutableSet setWithObjects:@"text/calendar", @"text/json", @"text/plain", @"text/javascript", @"application/json", nil];
    
    self.manager.requestSerializer = requestSerializer;
    self.manager.responseSerializer = responseSerializer;
    
    NSString *calendarString = [user.userName stringByReplacingOccurrencesOfString:ZIMBRA_EMAIL_TAG withString:@""];
    calendarURL = [NSString stringWithFormat:@"https://zimbratst.fortech.ro/home/%@/calendar?&auth=ba", calendarString];
    
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    [self.manager POST:calendarURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        dispatch_async(queue, ^{
            [self uploadSuccess:booking attachment:icsString method:requestType status:status isEditing:isEditing];
        });
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        dispatch_async(queue, ^{
            [self failedWithError:error];
        });
        
    }];
}

- (void)downloadMyBookings
{
    queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    User *user = [[UserManager sharedInstance] getActiveUser];
    NSString *calendar = [user.userName stringByReplacingOccurrencesOfString:ZIMBRA_EMAIL_TAG withString:@""];
    calendarURL = [NSString stringWithFormat:@"https://zimbratst.fortech.ro/home/%@/calendar?&auth=ba", calendar];
    
    [self.manager.requestSerializer setAuthorizationHeaderFieldWithUsername:user.userName password:user.password];
    
    [self.manager GET:calendarURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [self downloadAllBookingsUsingJSON];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        dispatch_async(queue, ^{
            [self signInFailedWithError:error];
        });
    
    }];
}


#pragma mark - JSON operations

- (void)downloadAllBookingsUsingJSON
{
    NSLog(@"JSON CALL FOR ALL BOOKINGS");
    queue = dispatch_queue_create("Operation queue", NULL);
    __block NSInteger conditionLock = 0;
    
    [dataArray removeAllObjects];
    
    for (Location *location in [LocationsManager sharedInstance].locations) {
        NSString *loginName = [[location.name stringByAppendingString:ZIMBRA_EMAIL_TAG] lowercaseString];
        loginName = [loginName stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *calendarString = [location.name lowercaseString];
        calendarString = [calendarString stringByReplacingOccurrencesOfString:@" " withString:@""];
        NSString *password = @"test";
        
        NSString *currentURL = [NSString stringWithFormat:@"https://zimbratst.fortech.ro/home/%@/calendar?fmt=json", calendarString];
        
        self.manager.requestSerializer = [AFJSONRequestSerializer serializer];
        [self.manager.requestSerializer setAuthorizationHeaderFieldWithUsername:loginName password:password];
        
        [self.manager GET:currentURL parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
            
            dispatch_async(queue, ^{
                [DataParser parseBookingsUsingJSON:responseObject toArray:dataArray];
                conditionLock ++;
                if (conditionLock == [LocationsManager sharedInstance].locations.count) {
                    [self finishedParsing];
                }
            });
            
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            
            [self failedWithError:error];
            
        }];
    }
}

#pragma mark - Private methods

- (void)finishedParsing
{
    if ([self.dataRetrieverDelegate respondsToSelector:@selector(downloadDidFinish:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.dataRetrieverDelegate downloadDidFinish:dataArray];
        });
    }
}

- (void)uploadSuccess:(Booking *)booking attachment:(NSString *)icsString method:(NSString *)method status:(NSString *)status isEditing:(BOOL)isEditing
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[EmailServiceManager sharedInstance] sendEmailWithRequest:booking attachment:icsString method:method status:status isEditing:isEditing];
    });
}

- (void)signInFailedWithError:(NSError *)error
{
    if ([self.dataRetrieverDelegate respondsToSelector:@selector(signInFailedWithError:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.dataRetrieverDelegate signInFailedWithError:error];
        });
    }
}

- (void)failedWithError:(NSError *)error
{
    if ([self.dataRetrieverDelegate respondsToSelector:@selector(requestFailedWithError:)]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self cancelService];
            [self.dataRetrieverDelegate requestFailedWithError:error];
        });
    }
}

- (void)cancelService
{
    [self.manager.operationQueue cancelAllOperations];
}

@end
