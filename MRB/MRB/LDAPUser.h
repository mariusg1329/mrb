//
//  LDAPUser.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/21/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LDAPUser : NSObject <NSCoding>

@property (nonatomic, strong) NSString *LDAPUsername;
@property (nonatomic, strong) NSString *LDAPPassword;
@property (nonatomic) BOOL isUsingLDAP;
@property (nonatomic) BOOL isUsingOffline;

@end
