//
//  GeneralUtils.h
//  MRB
//
//  Created by Ghita Marius Lucian on 26/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GeneralUtils : NSObject

/**
 Format a given adding a suffix
 @param date - The date to be formatted
 */
+ (NSString *)daySuffixForDate:(NSDate *)date;

/**
 Returns the time of the given date in a basic format
 @param date - The date to be formatted
 */
+ (NSString *)timeFromDate:(NSDate *)date;

/**
 Compare two dates by day/month/year
 @param date - The first date
 @param toDate - The date to which we compare the first date
 */
+ (BOOL)compareDate:(NSDate *)date toDate:(NSDate *)toDate;

/**
 Trimm the date by setting its time to 0
 @param date - The date to be trimmed
 */
+ (NSDate *)trimmedDateForDate:(NSDate *)date;

/**
 The date to be transformed to the last second of the day
 @param date - The date to be extended
 */
+ (NSDate *)extendedDateForDate:(NSDate *)date;

/**
 This method creates a string by adding a suffix to it, from a given number
 @param number - The number for the output string
 */
+ (NSString*)counterFromInt: (NSUInteger)number;

/**
 This method removes the special characters, defined in Constants.h, from a string
 @param input - The input string
 */
+ (NSString*)removeSpecials:(NSString *)input;

/**
 This method capitalizes the first letter of a string
 @param input - The input string
 */
+ (NSString*)upperCaseFirstLetter:(NSString*)input;


/**
 This method adds a view to the given controller's view in order to block the UI
 @param currentVC - The viewController who's view will be blocked
 */
+ (void)showLoadingOn:(UIViewController *)currentVC;

/**
 This method removes the UI blocking view from the controller's view
 @param currentVC - The viewController who's view will be unblocked
 */
+ (void)hideLoadingOn:(UIViewController *)currentVC;

+ (void)showLoadingOnView:(UIView *)view;
+ (void)hideLoadingOnView:(UIView *)view;
+ (void)reloadTable:(UITableView *)table animated:(BOOL)animated;

@end
