//
//  NSMutableArray+Bookings.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/7/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Booking;

@interface NSMutableArray (Bookings)

/**
 This method adds a booking to self ordered by date
 @param bookingToAdd - The booking to be added
 */
- (void)addBooking:(Booking *)bookingToAdd;

- (void)updateBooking:(Booking *)booking;

- (BOOL)containsBooking:(Booking *)aBooking;

@end
