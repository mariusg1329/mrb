//
//  OrderedDictionary+Bookings.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/8/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "OrderedDictionary+Bookings.h"
#import "GeneralUtils.h"

@implementation OrderedDictionary (Bookings)

- (void)insertObject:(id)anObject forLocation:(NSString *)locationName
{
    if ([self allKeys].count == 0) {
        [self insertObject:anObject forKey:locationName atIndex:0];
        return;
    }
    
    NSInteger index = 0;
    
    for (NSString *key in [self allKeys]) {
        if (![key isEqualToString:WFA_APPOINTMENTS_HEADER]) {
            if ([key localizedCaseInsensitiveCompare:locationName] == NSOrderedAscending) {
                index ++;
            }
        }
    }
    
    [self insertObject:anObject forKey:locationName atIndex:index];
}

- (void)insertObject:(id)anObject forDate:(NSDate *)date
{
    if ([self allKeys].count == 0) {
        [self insertObject:anObject forKey:date atIndex:0];
        return;
    }
        
    NSInteger index = 0;
    
    for (id keyDate in [self allKeys]) {
        if ([keyDate isKindOfClass:[NSDate class]]) {
            if ([date compare:keyDate]) {
                index ++;
            }
        }
    }
    
    [self insertObject:anObject forKey:date atIndex:index];
}

@end
