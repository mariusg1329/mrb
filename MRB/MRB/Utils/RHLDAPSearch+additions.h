//
//  RHLDAPSearch+additions.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/18/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "RHLDAPSearch.h"

@interface RHLDAPSearch (async)

- (void)searchWithQuery:(NSString *)query withinBase:(NSString *)base usingScope:(RHLDAPSearchScope)scope completionBlock:(dispatch_block_t)block;

@end
