//
//  RHLDAPSearch+additions.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/18/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "RHLDAPSearch+additions.h"

@implementation RHLDAPSearch (async)

- (void)searchWithQuery:(NSString *)query withinBase:(NSString *)base usingScope:(RHLDAPSearchScope)scope completionBlock:(dispatch_block_t)block
{
    [self.operationQueue cancelAllOperations];
    [self.operationQueue addOperationWithBlock:^{
            NSError *err;
            self.resultArray = [self searchWithQuery:query withinBase:base usingScope:scope error:&err];
            self.error = err;
            
            dispatch_async(dispatch_get_main_queue(), block);
    }];
}

@end
