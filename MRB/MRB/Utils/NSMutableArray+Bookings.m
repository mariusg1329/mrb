//
//  NSMutableArray+Bookings.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/7/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "NSMutableArray+Bookings.h"
#import "Booking.h"

@implementation NSMutableArray (Bookings)

- (void)addBooking:(Booking *)bookingToAdd
{
    if (self.count == 0) {
        [self insertObject:bookingToAdd atIndex:0];
        return;
    }
    
    NSInteger index = 0;
    
    for (Booking *booking in self) {
        
        if ([bookingToAdd.startDate compare:booking.startDate] == NSOrderedDescending) {
            index ++;
        }
        
    }
    
    [self insertObject:bookingToAdd atIndex:index];
}

- (BOOL)containsBooking:(Booking *)aBooking
{
    for (Booking *booking in self) {
        if ([booking.bookingId isEqualToString:aBooking.bookingId]) {
            return YES;
        }
    }
    
    return NO;
}

- (void)updateBooking:(Booking *)booking
{
    [self replaceObjectAtIndex:[self indexOfObject:booking] withObject:booking];
}

@end
