//
//  OrderedDictionary+Bookings.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/8/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "OrderedDictionary.h"

@interface OrderedDictionary (Bookings)

/**
 This method inserts anObject to self ordered by location string(key)
 @param anObject - The value object(array) to which will be added
 @param locationName - The given location string
 */
- (void)insertObject:(id)anObject forLocation:(NSString *)locationName;

/**
 This method inserts anObject to self ordered by date string(key)
 @param anObject - The value object(array) which will be added
 @param date - The given date string
 */
- (void)insertObject:(id)anObject forDate:(NSDate *)date;

@end
