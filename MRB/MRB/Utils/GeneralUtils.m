//
//  GeneralUtils.m
//  MRB
//
//  Created by Ghita Marius Lucian on 26/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "GeneralUtils.h"
#import "LoadingView.h"

@implementation GeneralUtils

#pragma mark - string and date Utils

+ (NSString *)daySuffixForDate:(NSDate *)date
{
    NSInteger day = [[[NSCalendar currentCalendar] components:NSDayCalendarUnit fromDate:date] day];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    NSString *dateString;
    
    switch (day) {
        case 1:
        case 21:
        case 31:
            [formatter setDateFormat:[NSString stringWithFormat:@"d'st of "]];
            dateString = [formatter stringFromDate:date];
            break;
        case 2:
        case 22:
            [formatter setDateFormat:[NSString stringWithFormat:@"d'nd of "]];
            dateString = [formatter stringFromDate:date];
            break;
        case 3:
        case 23:
            [formatter setDateFormat:[NSString stringWithFormat:@"d'rd of "]];
            dateString = [formatter stringFromDate:date];
            break;
        default:
            [formatter setDateFormat:[NSString stringWithFormat:@"d'th of "]];
            dateString = [formatter stringFromDate:date];
            break;
    }
    
    [formatter setDateFormat:@"MMMM YYYY"];
    NSString *finalString = [dateString stringByAppendingString:[formatter stringFromDate:date]];
    
    return finalString;
}

+ (NSString *)timeFromDate:(NSDate *)date
{
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm"];
    
    return [timeFormatter stringFromDate:date];
}

+ (NSDate *)trimmedDateForDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];

    return [calendar dateFromComponents:components];
}

+ (NSDate *)extendedDateForDate:(NSDate *)date
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSMinuteCalendarUnit | NSHourCalendarUnit | NSSecondCalendarUnit | NSDayCalendarUnit |NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];
    
    [components setHour:23];
    [components setMinute:59];
    [components setSecond:59];
    
    return [calendar dateFromComponents:components];
}

+ (BOOL)compareDate:(NSDate *)date toDate:(NSDate *)toDate
{
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:date];
    
    NSDate *firstDate = [calendar dateFromComponents:components];
    
    components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:toDate];

    NSDate *secondDate = [calendar dateFromComponents:components];
    
    return [firstDate compare:secondDate] == NSOrderedSame;
}

+ (NSString*)counterFromInt: (NSUInteger)number
{
    NSString *output = @"";
    
    number = number % 10;
    
    switch (number) {
        case 1:
            output = [NSString stringWithFormat:@"%lu'st",(unsigned long)number];
            break;
        case 2:
            output = [NSString stringWithFormat:@"%lu'nd",(unsigned long)number];
            break;
        case 3:
            output = [NSString stringWithFormat:@"%lu'rd",(unsigned long)number];
            break;
        default:
            output = [NSString stringWithFormat:@"%lu'th",(unsigned long)number];
            break;
    }

    return output;
}

+ (NSString*)removeSpecials:(NSString *)input
{
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:SPECIAL_CHARACTERS
                                                                                options:0
                                                                                  error:NULL];
    NSString *output = [expression stringByReplacingMatchesInString:input
                                                    options:0
                                                    range:NSMakeRange(0, input.length)
                                                    withTemplate:@""];
    output = [output stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    output = [output stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]];
    
    return output;
}

+ (NSString*)upperCaseFirstLetter:(NSString*)input
{
    NSString *result;
    
    result = [input substringToIndex:1];
    result = [result uppercaseString];
    
    return  [input stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:result];
}

#pragma mark - UI Utils

+ (void)showLoadingOn:(UIViewController *)viewController
{
    LoadingView *loading = [[LoadingView alloc] initWithFrame:viewController.view.frame];
    
    [viewController.view setUserInteractionEnabled:NO];
    [viewController.view addSubview:loading];
}

+ (void)hideLoadingOn:(UIViewController *)viewController
{
    for (UIView *subView in viewController.view.subviews) {
        if ([subView isKindOfClass:[LoadingView class]]) {
                [subView removeFromSuperview];
        }
    }
    
    [viewController.view setUserInteractionEnabled:YES];
}

+ (void)showLoadingOnView:(UIView *)view
{
    LoadingView *loading = [[LoadingView alloc] initWithFrame:view.frame];
    [view addSubview:loading];
}

+ (void)hideLoadingOnView:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[LoadingView class]]) {
            [subView removeFromSuperview];
        }
    }
}

+ (void)reloadTable:(UITableView *)table animated:(BOOL)animated
{
    if (animated) {
        [UIView transitionWithView: table
                          duration: 0.35f
                           options: UIViewAnimationOptionTransitionCrossDissolve
                        animations: ^(void)
         {
             [table reloadData];
         }
         
                        completion: nil];
    } else {
        [table reloadData];
    }
    
}

@end
