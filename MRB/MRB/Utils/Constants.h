//
//  Constants.h
//  MRB
//
//  Created by Ghita Marius Lucian on 8/30/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#ifndef MRB_Constants_h
#define MRB_Constants_h

#define IOS_7 7.1f

//UI STUFF
#define MAX_DISPLAYED_ATTENDEES 2
#define CELL_HEIGHT 50
#define ATTENDEES_CELL_HEIGHT 35
#define HEADER_HEIGHT 50
#define KEYBOARD_HEIGHT 216 //ONLY FOR IPHONE
#define DEFAULT_LEFT_PADDING 20
#define DEFAULT_RIGHT_PADDING 20
#define DEFAULT_BUTTON_HEIGHT 30
#define DEFAULT_BUTTON_WIDTH 30
#define SPACING 5
#define LDAP_CONTAINER_HEIGHT 112
#define GENERAL_PADDING 15
#define MAX_DISPLAYED_RESULTS (SCREEN_HEIGHT > 480.0f ? 5 : 3)

//SCREEN HEIGHT
#define IS_PHONE UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone
#define IS_WIDESCREEN (fabs((double)[[UIScreen mainScreen] bounds].size.height - (double)568) < DBL_EPSILON)
#define SCREEN_HEIGHT ((IS_PHONE) ? (IS_WIDESCREEN) ? 568.0f : 480.0f : 512.0f)

//NOT UI STUFF
#define ZIMBRA_EMAIL_TAG @"@zimbratst.fortech.ro"
#define SPECIAL_CHARACTERS @"[,\\.`\"/;:<>=]"
#define DEFAULT_ERROR_CODE -1011
#define ZIMBRA_SMTP_SERVER @"zimbratst.fortech.ro"
#define LDAP_SERVER @"ldap://ldap.fortech.ro"

//NOTIFICATION STUFF
#define WS_ERROR_CONNECTING @"Failed connecting"
#define WS_ERROR_AUTHENTIFICATION @"Authentification failed"
#define WS_ERROR_TIMEOUT @"Connection timeout!"
#define WS_ERROR_NO_ITEM @"There is no calendar for this account"
#define WS_ERROR_SERVER_NOT_FOUND @"The server is down, please try again later"
#define WS_ERROR_NO_INTERNET @"The internet connection appears to be offline"

//SIGN IN ALERTS
#define SIGN_IN_ALERT_SIGNIN @"This section requires you to be signed in."
#define SIGN_IN_MY_APPOINTMENTS @"You must sign in to manage your own appointments."

//MY APPOINTMENTS
#define MY_APPOINTMENTS_EMPTY @"You have no appointments created."
#define WFA_APPOINTMENTS_HEADER @"Waiting for approval"

//ALL APPOINTMENTS
#define ALL_APPOINTMENTS_EMTPY @"There are no appointments.\nPull down to refresh."

//EDITABLE APPOINTMENT
#define EMAIL_SEARCH_CHARACTERS @"Type more than 2 characters for the search to begin."
#define EMAIL_SEARCH_LDAP @"To enable this search please set your LDAP credentials from the settings tab."

#endif
