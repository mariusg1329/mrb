//
//  AppDelegate.m
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "AppDelegate.h"
#import "AFNetworkActivityLogger.h"
#import "UserManager+Cache.h"
#import "Location.h"
#import "BookingsManager.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    [self.window setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"GeneralBackgroundImage"]]];
    
    float ver = [[UIDevice currentDevice].systemVersion floatValue];
    
    NSDictionary *textTitleOptions = [NSDictionary dictionaryWithObjectsAndKeys:[UIColor colorWithRed:31/255 green:40/255 blue:86/255 alpha:1], UITextAttributeTextColor, nil];
    [[UIBarButtonItem appearance] setTitleTextAttributes:textTitleOptions forState:UIControlStateNormal];
    [[UIBarButtonItem appearance] setTitleTextAttributes:textTitleOptions forState:UIControlStateHighlighted];
    
    if (ver < IOS_7) {
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
        [UINavigationBar appearance].shadowImage = [[UIImage alloc] init];
        [[UINavigationBar appearance] setTitleTextAttributes:textTitleOptions];
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleBlackOpaque;
    }
    
    [UITabBar appearance].backgroundImage = [UIImage imageNamed:@"login_480x60.png"];
    [UITabBar appearance].tintColor = [UIColor whiteColor];
    
    //LOG WS ACTIVITY
//    [[AFNetworkActivityLogger sharedLogger] startLogging];
//    [[AFNetworkActivityLogger sharedLogger] setLevel:(AFLoggerLevelDebug)];
    
    [self startupWithLocations];
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[[UserManager sharedInstance] getActiveUser]];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"user"];
}

- (void)startupWithLocations
{
    // 1st Floor - Rivers' Floor
    Location *location = [[Location alloc] init];
    
    location.name = @"Danube";
    location.size = @"S";
    location.floor = GroundFloor;
    
    Location *location1 = [[Location alloc] init];
    
    location1.name = @"Ganges";
    location1.size = @"S";
    location1.floor = GroundFloor;
    
    Location *location2 = [[Location alloc] init];
    
    location2.name = @"Rhine";
    location2.size = @"M";
    location2.floor = GroundFloor;
    
    Location *location3 = [[Location alloc] init];
    
    location3.name = @"Amazon";
    location3.size = @"L";
    location3.floor = GroundFloor;
    
    
    // 2nd Floor - Mountains' (Peaks') Floor
    Location *location4 = [[Location alloc] init];
    
    location4.name = @"Negoiu";
    location4.size = @"S";
    location4.floor = FirstFloor;
    
    Location *location5 = [[Location alloc] init];
    
    location5.name = @"Mont Blanc";
    location5.size = @"S";
    location5.floor = FirstFloor;
    
    Location *l6 = [[Location alloc] init];
    
    l6.name = @"Fuji";
    l6.size = @"M";
    l6.floor = FirstFloor;
    
    Location *l7 = [[Location alloc] init];
    
    l7.name = @"K2";
    l7.size = @"L";
    l7.floor = FirstFloor;
    
    
    // 4th Floor - Museums' Floor
    Location *l8 = [[Location alloc] init];
    
    l8.name = @"Antipa";
    l8.size = @"S";
    l8.floor = ThirdFloor;
    
    Location *l9 = [[Location alloc] init];
    
    l9.name = @"Hermitage";
    l9.size = @"S";
    l9.floor = ThirdFloor;
    
    Location *l10 = [[Location alloc] init];
    
    l10.name = @"Louvre";
    l10.size = @"M";
    l10.floor = ThirdFloor;
    
    Location *l11 = [[Location alloc] init];
    
    l11.name = @"Batak";
    l11.size = @"L";
    l11.floor = ThirdFloor;
    
    
    // 3rd Floor - Cities' Floor
    Location *l12 = [[Location alloc] init];
    
    l12.name = @"Napoca";
    l12.size = @"S";
    l12.floor = SecondFloor;
    
    Location *l13 = [[Location alloc] init];
    
    l13.name = @"Bruges";
    l13.size = @"S";
    l13.floor = SecondFloor;
    
    Location *l14 = [[Location alloc] init];
    
    l14.name = @"Beijing";
    l14.size = @"M";
    l14.floor = SecondFloor;
    
    Location *l15 = [[Location alloc] init];
    
    l15.name = @"Detroit";
    l15.size = @"L";
    l15.floor = SecondFloor;
    
    // 5th Floor
    Location *l16 = [[Location alloc] init];
    
    l16.name = @"Sapphire";
    l16.size = @"XL";
    l16.floor = FourthFloor;
    
    Location *l17 = [[Location alloc] init];
    
    l17.name = @"Board Room";
    l17.size = @"XXXL";
    l17.floor = FourthFloor;
    
    [LocationsManager sharedInstance].locations = [[NSMutableArray alloc] initWithArray:@[location, location1, location2, location3, location4, location5,l6,l7,l8,l9,l10,l11,l12,l13,l14,l15,l16,l17]];
}

@end
