//
//  Booking.h
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Location.h"
@interface Booking : NSObject

@property (nonatomic, strong) NSString *bookingId;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *organizer;
@property (nonatomic, strong) NSMutableArray *attendees;
@property (nonatomic, strong) NSDate *startDate;
@property (nonatomic, strong) NSDate *endDate;
@property (nonatomic, strong) Location *location;
@property (nonatomic) BOOL isApproved;
@property (nonatomic) BOOL isBeingCanceled;
@property (nonatomic) BOOL isBeingEdited;

- (id)initWithData:(NSString*)startDateTimeString
             endDate:(NSString*)endDateTimeString
            uniqueID:(NSString*)eventUniqueIDString
             summary:(NSString*)summaryString
         description:(NSString*)descriptionString
            location:(NSString*)locationString
         organizer:(NSString*)organizer
           attendees:(NSMutableArray*)attendees;

- (BOOL)isEqualByID:(Booking *)booking;

@end
