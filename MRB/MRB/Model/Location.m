//
//  Location.m
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "Location.h"

@implementation Location

- (id)init
{
    if (self = [super init]) {
        _name = @"";
        _size = @"";
        _floor = GroundFloor;
    }
    
    return self;
}

- (id)initWithRoomName:(NSString *)roomName
{
    if (self = [super init]) {
        
        if ([roomName isEqualToString:@"Danube"]) {
            _name = @"Danube";
            _size = @"S";
            _floor = GroundFloor;
        } else if ([roomName isEqualToString:@"Ganges"]) {
            _name = @"Ganges";
            _size = @"S";
            _floor = GroundFloor;
        } else if ([roomName isEqualToString:@"Rhine"]) {
            _name = @"Rhine";
            _size = @"M";
            _floor = GroundFloor;
        } else if ([roomName isEqualToString:@"Amazon"]) {
            _name = @"Amazon";
            _size = @"L";
            _floor = GroundFloor;
        } else if ([roomName isEqualToString:@"Negoiu"]) {
            _name = @"Negoiu";
            _size = @"S";
            _floor = FirstFloor;
        } else if ([roomName isEqualToString:@"Montblanc"]) {
            _name = @"Mont Blanc";
            _size = @"S";
            _floor = FirstFloor;
        } else if ([roomName isEqualToString:@"Fuji"]) {
            _name = @"Fuji";
            _size = @"M";
            _floor = FirstFloor;
        } else if ([roomName isEqualToString:@"K2"]) {
            _name = @"K2";
            _size = @"L";
            _floor = FirstFloor;
        } else if ([roomName isEqualToString:@"Napoca"]) {
            _name = @"Napoca";
            _size = @"S";
            _floor = SecondFloor;
        } else if ([roomName isEqualToString:@"Bruges"]) {
            _name = @"Bruges";
            _size = @"S";
            _floor = SecondFloor;
        } else if ([roomName isEqualToString:@"Beijing"]) {
            _name = @"Beijing";
            _size = @"M";
            _floor = SecondFloor;
        } else if ([roomName isEqualToString:@"Detroit"]) {
            _name = @"Detroit";
            _size = @"L";
            _floor = SecondFloor;
        } else if ([roomName isEqualToString:@"Antipa"]) {
            _name = @"Antipa";
            _size = @"S";
            _floor = ThirdFloor;
        } else if ([roomName isEqualToString:@"Hermitage"]) {
            _name = @"Hermitage";
            _size = @"S";
            _floor = ThirdFloor;
        } else if ([roomName isEqualToString:@"Louvre"]) {
            _name = @"Louvre";
            _size = @"M";
            _floor = ThirdFloor;
        } else if ([roomName isEqualToString:@"Batak"]) {
            _name = @"Batak";
            _size = @"L";
            _floor = ThirdFloor;
        } else if ([roomName isEqualToString:@"Sapphire"]) {
            _name = @"Sapphire";
            _size = @"XL";
            _floor = FourthFloor;
        } else if ([roomName isEqualToString:@"Boardroom"]) {
            _name = @"Board Room";
            _size = @"XXXL";
            _floor = FourthFloor;
        } else {
            _name = roomName;
            _size = @"?";
            _floor = FourthFloor;
        }
        
    }
    
    return self;
}

@end
