//
//  User.h
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject <NSCoding>

@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;

@end
