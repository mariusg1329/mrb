//
//  User.m
//  MRB
//
//  Created by Ghita Marius Lucian on 9/6/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "User.h"

@implementation User

- (id)initWithCoder:(NSCoder *)decoder
{
    if (self = [super init]) {
        self.userName = [decoder decodeObjectForKey:@"userName"];
        self.password = [decoder decodeObjectForKey:@"password"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.userName forKey:@"userName"];
    [encoder encodeObject:self.password forKey:@"password"];
}

@end
