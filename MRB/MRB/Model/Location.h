//
//  Location.h
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocationsManager.h"

@interface Location : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic) BuildingFloor floor;
@property (nonatomic, strong) NSString* size;

- (id)initWithRoomName:(NSString *)roomName;

@end
