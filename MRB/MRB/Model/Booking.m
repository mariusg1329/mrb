//
//  Booking.m
//  MRB
//
//  Created by Ghita Marius Lucian on 17/07/14.
//  Copyright (c) 2014 work. All rights reserved.
//

#import "Booking.h"
#import "UserManager.h"

@implementation Booking

@synthesize description = _description;

- (id)init
{
    if (self = [super init]) {
        _bookingId = [[NSUUID UUID] UUIDString];
        _description = @"";
        _title = @"B";
        _organizer = [[UserManager sharedInstance] getActiveUser].userName;
        _startDate = [[NSDate alloc] init];
        _endDate = [[NSDate alloc] init];
        _attendees = [[NSMutableArray alloc] init];
        _location = [[Location alloc] init];
        _isApproved = NO;
        _isBeingCanceled = NO;
        _isBeingEdited = NO;
    }
    
    return self;
}

- (id)initWithData:(NSString*)startDateTimeString
     endDate:(NSString*)endDateTimeString
     uniqueID:(NSString*)eventUniqueIDString
     summary:(NSString*)summaryString
     description:(NSString*)descriptionString
     location:(NSString*)locationString
         organizer:(NSString*)organizer
         attendees:(NSMutableArray*)attendees {
    
    if (self = [super init]) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        dateFormat.dateFormat = @"yyyyMMdd'T'HHmmss";
        
        _bookingId = eventUniqueIDString;
        
        if (!descriptionString) {
            _description = @"";
        } else {
            _description = descriptionString;
        }
        
        _title = summaryString;
        _organizer = organizer;
        _startDate = [dateFormat dateFromString:startDateTimeString];
        _endDate = [dateFormat dateFromString:endDateTimeString];
        _attendees = attendees;
        _location = [[Location alloc] initWithRoomName:locationString];
        _isApproved = YES;
    }
    
    return self;
}

- (BOOL)isEqual:(id)object
{
    Booking *booking = (Booking *)object;
    
    if ([self.title isEqualToString:booking.title] &&
        [self.bookingId isEqualToString:booking.bookingId] &&
        [self.description isEqualToString:booking.description] &&
        [self.location.name isEqualToString:booking.location.name] &&
        [self.startDate compare:booking.startDate] == NSOrderedSame &&
        [self.endDate compare:booking.startDate] == NSOrderedSame &&
        [self.organizer isEqualToString:booking.organizer]){
        return YES;
    }
    return NO;
}

- (BOOL)isEqualByID:(Booking *)booking
{
    return [self.bookingId isEqualToString:booking.bookingId];
}

@end
